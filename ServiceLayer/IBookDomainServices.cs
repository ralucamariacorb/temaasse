﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBookDomainServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IBookDomainDomainServices interface.
    /// </summary>
    public interface IBookDomainServices
    {
        /// <summary>
        /// Gets all bookDomains.
        /// </summary>
        /// <returns>All bookDomains.</returns>
        IList<BookDomain> GetAllBookDomains();

        /// <summary>
        /// Adds the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void AddBookDomain(BookDomain bookDomain);

        /// <summary>
        /// Deletes the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void DeleteBookDomain(BookDomain bookDomain);

        /// <summary>
        /// Updates the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void UpdateBookDomain(BookDomain bookDomain);
    }
}