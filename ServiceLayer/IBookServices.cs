﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBookServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IBookServices interface.
    /// </summary>
    public interface IBookServices
    {
        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns>All books.</returns>
        IList<Book> GetAllBooks();

        /// <summary>
        /// Adds the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void AddBook(Book book);

        /// <summary>
        /// Deletes the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void DeleteBook(Book book);

        /// <summary>
        /// Updates the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void UpdateBook(Book book);

        /// <summary>
        /// Deletes the book by given id.
        /// </summary>
        /// <param name="id">The book id.</param>
        void DeleteBookById(int id);

        /// <summary>
        /// Gets the book by given id.
        /// </summary>
        /// <param name="id">The book id.</param>
        /// <returns>The book or null if there is no book with given id.</returns>
        Book GetBookById(int id);
    }
}
