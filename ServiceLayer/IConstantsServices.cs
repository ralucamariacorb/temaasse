﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConstantsServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IConstantsServices interface.
    /// </summary>
    public interface IConstantsServices
    {
        /// <summary>
        /// Gets all constants.
        /// </summary>
        /// <returns>All constants.</returns>
        IList<Constants> GetAllConstants();

        /// <summary>
        /// Adds the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        void AddConstants(Constants constants);

        /// <summary>
        /// Deletes the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        void DeleteConstants(Constants constants);

        /// <summary>
        /// Updates the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        void UpdateConstants(Constants constants);

        /// <summary>
        /// Deletes the constants by given id.
        /// </summary>
        /// <param name="id">The constants id.</param>
        void DeleteConstantsById(int id);

        /// <summary>
        /// Gets the constants by given id.
        /// </summary>
        /// <param name="id">The constants id.</param>
        /// <returns>The constants or null if there is no constants with given id.</returns>
        Constants GetConstantsById(int id);

        /// <summary>
        /// Gets the latest constants.
        /// </summary>
        /// <returns>The constants or null if there is no constants.</returns>
        Constants GetLatestConstants();
    }
}