﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDomainServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IDomainServices interface.
    /// </summary>
    public interface IDomainServices
    {
        /// <summary>
        /// Gets all domains.
        /// </summary>
        /// <returns>All domains.</returns>
        IList<Domain> GetAllDomains();

        /// <summary>
        /// Adds the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        void AddDomain(Domain domain);

        /// <summary>
        /// Deletes the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        void DeleteDomain(Domain domain);

        /// <summary>
        /// Updates the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        void UpdateDomain(Domain domain);

        /// <summary>
        /// Deletes the domain by given id.
        /// </summary>
        /// <param name="id">The domain id.</param>
        void DeleteDomainById(int id);

        /// <summary>
        /// Gets the domain by given id.
        /// </summary>
        /// <param name="id">The domain id.</param>
        /// <returns>The domain or null if there is no domain with given id.</returns>
        Domain GetDomainById(int id);
    }
}