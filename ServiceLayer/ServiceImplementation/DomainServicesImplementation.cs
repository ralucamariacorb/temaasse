﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DomainServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// DomainServicesImplementation class.
    /// </summary>
    public class DomainServicesImplementation : IDomainServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IDomainDataServices.
        /// </summary>
        private static IDomainDataServices iDomainDataServices;

        /// <summary>
        /// Domain validator.
        /// </summary>
        private static DomainValidator validator = new DomainValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="domainDataServices">domainDataServices.</param>
        public DomainServicesImplementation(ILog log, IDomainDataServices domainDataServices)
        {
            iLog = log;
            iDomainDataServices = domainDataServices;
        }

        /// <summary>
        /// Adds the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void AddDomain(Domain domain)
        {
            ValidationResult result = validator.Validate(domain);
            if (result.IsValid)
            {
                iDomainDataServices.AddDomain(domain);
                iLog.Info($"The domain with id {domain.Id} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add domain because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void DeleteDomain(Domain domain)
        {
            try
            {
                iDomainDataServices.DeleteDomain(domain);
                iLog.Info($"The domain with id {domain.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all domains.
        /// </summary>
        /// <returns>All domains.</returns>
        public IList<Domain> GetAllDomains()
        {
            return iDomainDataServices.GetAllDomains();
        }

        /// <summary>
        /// Updates the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void UpdateDomain(Domain domain)
        {
            ValidationResult result = validator.Validate(domain);
            if (result.IsValid)
            {
                try
                {
                    iDomainDataServices.UpdateDomain(domain);
                    iLog.Info($"The domain with id {domain.Id} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update domain because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the domain by given id.
        /// </summary>
        /// <param name="id">The domain id.</param>
        public void DeleteDomainById(int id)
        {
            var domain = this.GetDomainById(id);
            this.DeleteDomain(domain);
        }

        /// <summary>
        /// Gets the domain by given id.
        /// </summary>
        /// <param name="id">The domain id.</param>
        /// <returns>The domain or null if there is no domain with given id.</returns>
        public Domain GetDomainById(int id)
        {
            var domain = iDomainDataServices.GetDomainById(id);
            if (domain != null)
            {
                iLog.Info($"The domain with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The domain with id {id} does not exist.");
            }

            return domain;
        }
    }
}