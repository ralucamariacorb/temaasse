﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthorServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// AuthorServicesImplementation class.
    /// </summary>
    public class AuthorServicesImplementation : IAuthorServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IAuthorDataServices.
        /// </summary>
        private static IAuthorDataServices iAuthorDataServices;

        /// <summary>
        /// Author validator.
        /// </summary>
        private static AuthorValidator validator = new AuthorValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="authorDataServices">authorDataServices.</param>
        public AuthorServicesImplementation(ILog log, IAuthorDataServices authorDataServices)
        {
            iLog = log;
            iAuthorDataServices = authorDataServices;
        }

        /// <summary>
        /// Adds the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void AddAuthor(Author author)
        {
            ValidationResult result = validator.Validate(author);
            if (result.IsValid)
            {
                iAuthorDataServices.AddAuthor(author);
                iLog.Info($"The author with id {author.Id} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add author because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void DeleteAuthor(Author author)
        {
            try
            {
                iAuthorDataServices.DeleteAuthor(author);
                iLog.Info($"The author with id {author.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all authors.
        /// </summary>
        /// <returns>All authors.</returns>
        public IList<Author> GetAllAuthors()
        {
            return iAuthorDataServices.GetAllAuthors();
        }

        /// <summary>
        /// Updates the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void UpdateAuthor(Author author)
        {
            ValidationResult result = validator.Validate(author);
            if (result.IsValid)
            {
                try
                {
                    iAuthorDataServices.UpdateAuthor(author);
                    iLog.Info($"The author with id {author.Id} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update author because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the author by given id.
        /// </summary>
        /// <param name="id">The author id.</param>
        public void DeleteAuthorById(int id)
        {
            var author = this.GetAuthorById(id);
            this.DeleteAuthor(author);
        }

        /// <summary>
        /// Gets the author by given id.
        /// </summary>
        /// <param name="id">The author id.</param>
        /// <returns>The author or null if there is no author with given id.</returns>
        public Author GetAuthorById(int id)
        {
            var author = iAuthorDataServices.GetAuthorById(id);
            if (author != null)
            {
                iLog.Info($"The author with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The author with id {id} does not exist.");
            }

            return author;
        }
    }
}
