﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantsServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// ConstantsServicesImplementation class.
    /// </summary>
    public class ConstantsServicesImplementation : IConstantsServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IConstantsDataServices.
        /// </summary>
        private static IConstantsDataServices iConstantsDataServices;

        /// <summary>
        /// Constants validator.
        /// </summary>
        private static ConstantsValidator validator = new ConstantsValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="ConstantsServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="constantsDataServices">constantsDataServices.</param>
        public ConstantsServicesImplementation(ILog log, IConstantsDataServices constantsDataServices)
        {
            iLog = log;
            iConstantsDataServices = constantsDataServices;
        }

        /// <summary>
        /// Adds the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void AddConstants(Constants constants)
        {
            ValidationResult result = validator.Validate(constants);
            if (result.IsValid)
            {
                if (iConstantsDataServices.GetAllConstants().Count > 0)
                {
                    var oldConstants = iConstantsDataServices.GetLatestConstants();
                    this.DeleteConstants(oldConstants);
                }

                iConstantsDataServices.AddConstants(constants);
                iLog.Info($"The constants with id {constants.Id} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add constants because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void DeleteConstants(Constants constants)
        {
            try
            {
                iConstantsDataServices.DeleteConstants(constants);
                iLog.Info($"The constants with id {constants.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all constants.
        /// </summary>
        /// <returns>All constants.</returns>
        public IList<Constants> GetAllConstants()
        {
            return iConstantsDataServices.GetAllConstants();
        }

        /// <summary>
        /// Updates the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void UpdateConstants(Constants constants)
        {
            ValidationResult result = validator.Validate(constants);
            if (result.IsValid)
            {
                try
                {
                    iConstantsDataServices.UpdateConstants(constants);
                    iLog.Info($"The constants with id {constants.Id} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update constants because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the constants by given id.
        /// </summary>
        /// <param name="id">The constants id.</param>
        public void DeleteConstantsById(int id)
        {
            var constants = this.GetConstantsById(id);
            this.DeleteConstants(constants);
        }

        /// <summary>
        /// Gets the constants by given id.
        /// </summary>
        /// <param name="id">The constants id.</param>
        /// <returns>The constants or null if there is no constants with given id.</returns>
        public Constants GetConstantsById(int id)
        {
            var constants = iConstantsDataServices.GetConstantsById(id);
            if (constants != null)
            {
                iLog.Info($"The constants with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The constants with id {id} does not exist.");
            }

            return constants;
        }

        /// <summary>
        /// Gets the latest constants.
        /// </summary>
        /// <returns>The constants or null if there is no constants.</returns>
        public Constants GetLatestConstants()
        {
            var constants = iConstantsDataServices.GetLatestConstants();
            if (constants != null)
            {
                iLog.Info($"There are constants in the database.");
            }
            else
            {
                iLog.Warn("There are not constants in the database.");
            }

            return constants;
        }
    }
}
