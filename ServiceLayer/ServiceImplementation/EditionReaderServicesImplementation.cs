﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditionReaderServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System.Collections.Generic;
    using DataMapper;
    using DomainModel;

    /// <summary>
    /// EditionReaderServicesImplementation class.
    /// </summary>
    public class EditionReaderServicesImplementation : IEditionReaderServices
    {
        /// <summary>
        /// Adds the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void AddEditionReader(EditionReader editionReader)
        {
            DAOFactoryMethod.CurrentDAOFactory.EditionReaderDataServices.AddEditionReader(editionReader);
        }

        /// <summary>
        /// Deletes the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void DeleteEditionReader(EditionReader editionReader)
        {
            DAOFactoryMethod.CurrentDAOFactory.EditionReaderDataServices.DeleteEditionReader(editionReader);
        }

        /// <summary>
        /// Gets all editionReaders.
        /// </summary>
        /// <returns>All bookReaders.</returns>
        public IList<EditionReader> GetAllEditionReaders()
        {
            return DAOFactoryMethod.CurrentDAOFactory.EditionReaderDataServices.GetAllEditionReaders();
        }

        /// <summary>
        /// Updates the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void UpdateEditionReader(EditionReader editionReader)
        {
            DAOFactoryMethod.CurrentDAOFactory.EditionReaderDataServices.UpdateEditionReader(editionReader);
        }
    }
}