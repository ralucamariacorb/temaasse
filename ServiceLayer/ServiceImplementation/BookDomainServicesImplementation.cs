﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookDomainServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System.Collections.Generic;
    using DataMapper;
    using DomainModel;

    /// <summary>
    /// BookDomainServicesImplementation class.
    /// </summary>
    public class BookDomainServicesImplementation : IBookDomainServices
    {
        /// <summary>
        /// Adds the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void AddBookDomain(BookDomain bookDomain)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookDomainDataServices.AddBookDomain(bookDomain);
        }

        /// <summary>
        /// Deletes the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void DeleteBookDomain(BookDomain bookDomain)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookDomainDataServices.DeleteBookDomain(bookDomain);
        }

        /// <summary>
        /// Gets all bookDomains.
        /// </summary>
        /// <returns>All bookDomains.</returns>
        public IList<BookDomain> GetAllBookDomains()
        {
            return DAOFactoryMethod.CurrentDAOFactory.BookDomainDataServices.GetAllBookDomains();
        }

        /// <summary>
        /// Updates the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void UpdateBookDomain(BookDomain bookDomain)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookDomainDataServices.UpdateBookDomain(bookDomain);
        }
    }
}