﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookAuthorServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System.Collections.Generic;
    using DataMapper;
    using DomainModel;

    /// <summary>
    /// BookAuthorServicesImplementation class.
    /// </summary>
    public class BookAuthorServicesImplementation : IBookAuthorServices
    {
        /// <summary>
        /// Adds the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void AddBookAuthor(BookAuthor bookAuthor)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookAuthorDataServices.AddBookAuthor(bookAuthor);
        }

        /// <summary>
        /// Deletes the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void DeleteBookAuthor(BookAuthor bookAuthor)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookAuthorDataServices.DeleteBookAuthor(bookAuthor);
        }

        /// <summary>
        /// Gets all bookAuthors.
        /// </summary>
        /// <returns>All bookAuthors.</returns>
        public IList<BookAuthor> GetAllBookAuthors()
        {
            return DAOFactoryMethod.CurrentDAOFactory.BookAuthorDataServices.GetAllBookAuthors();
        }

        /// <summary>
        /// Updates the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void UpdateBookAuthor(BookAuthor bookAuthor)
        {
            DAOFactoryMethod.CurrentDAOFactory.BookAuthorDataServices.UpdateBookAuthor(bookAuthor);
        }
    }
}
