﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReaderServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// ReaderServicesImplementation class.
    /// </summary>
    public class ReaderServicesImplementation : IReaderServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IReaderDataServices.
        /// </summary>
        private static IReaderDataServices iReaderDataServices;

        /// <summary>
        /// Reader validator.
        /// </summary>
        private static ReaderValidator validator = new ReaderValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="readerDataServices">readerDataServices.</param>
        public ReaderServicesImplementation(ILog log, IReaderDataServices readerDataServices)
        {
            iLog = log;
            iReaderDataServices = readerDataServices;
        }

        /// <summary>
        /// Adds the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void AddReader(Reader reader)
        {
            ValidationResult result = validator.Validate(reader);
            if (result.IsValid)
            {
                iReaderDataServices.AddReader(reader);
                iLog.Info($"The reader with id {reader.Id} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add reader because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void DeleteReader(Reader reader)
        {
            try
            {
                iReaderDataServices.DeleteReader(reader);
                iLog.Info($"The reader with id {reader.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all readers.
        /// </summary>
        /// <returns>All readers.</returns>
        public IList<Reader> GetAllReaders()
        {
            return iReaderDataServices.GetAllReaders();
        }

        /// <summary>
        /// Updates the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void UpdateReader(Reader reader)
        {
            ValidationResult result = validator.Validate(reader);
            if (result.IsValid)
            {
                try
                {
                    iReaderDataServices.UpdateReader(reader);
                    iLog.Info($"The reader with id {reader.Id} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update reader because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the reader by given id.
        /// </summary>
        /// <param name="id">The reader id.</param>
        public void DeleteReaderById(int id)
        {
            var reader = this.GetReaderById(id);
            this.DeleteReader(reader);
        }

        /// <summary>
        /// Gets the reader by given id.
        /// </summary>
        /// <param name="id">The reader id.</param>
        /// <returns>The reader or null if there is no reader with given id.</returns>
        public Reader GetReaderById(int id)
        {
            var reader = iReaderDataServices.GetReaderById(id);
            if (reader != null)
            {
                iLog.Info($"The reader with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The reader with id {id} does not exist.");
            }

            return reader;
        }
    }
}
