﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditionServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// EditionServicesImplementation class.
    /// </summary>
    public class EditionServicesImplementation : IEditionServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IEditionDataServices.
        /// </summary>
        private static IEditionDataServices iEditionDataServices;

        /// <summary>
        /// Edition validator.
        /// </summary>
        private static EditionValidator validator = new EditionValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="EditionServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="editionDataServices">editionDataServices.</param>
        public EditionServicesImplementation(ILog log, IEditionDataServices editionDataServices)
        {
            iLog = log;
            iEditionDataServices = editionDataServices;
        }

        /// <summary>
        /// Adds the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void AddEdition(Edition edition)
        {
            ValidationResult result = validator.Validate(edition);
            if (result.IsValid)
            {
                iEditionDataServices.AddEdition(edition);
                iLog.Info($"The edition with id {edition.Id} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add edition because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void DeleteEdition(Edition edition)
        {
            try
            {
                iEditionDataServices.DeleteEdition(edition);
                iLog.Info($"The edition with id {edition.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all editions.
        /// </summary>
        /// <returns>All editions.</returns>
        public IList<Edition> GetAllEditions()
        {
            return iEditionDataServices.GetAllEditions();
        }

        /// <summary>
        /// Updates the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void UpdateEdition(Edition edition)
        {
            ValidationResult result = validator.Validate(edition);
            if (result.IsValid)
            {
                try
                {
                    iEditionDataServices.UpdateEdition(edition);
                    iLog.Info($"The edition with id {edition.Id} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update edition because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the edition by given id.
        /// </summary>
        /// <param name="id">The edition id.</param>
        public void DeleteEditionById(int id)
        {
            var edition = this.GetEditionById(id);
            this.DeleteEdition(edition);
        }

        /// <summary>
        /// Gets the edition by given id.
        /// </summary>
        /// <param name="id">The edition id.</param>
        /// <returns>The edition or null if there is no edition with given id.</returns>
        public Edition GetEditionById(int id)
        {
            var edition = iEditionDataServices.GetEditionById(id);
            if (edition != null)
            {
                iLog.Info($"The edition with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The edition with id {id} does not exist.");
            }

            return edition;
        }
    }
}