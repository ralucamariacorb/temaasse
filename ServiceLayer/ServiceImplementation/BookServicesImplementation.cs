﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// BookServicesImplementation class.
    /// </summary>
    public class BookServicesImplementation : IBookServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// IBookDataServices.
        /// </summary>
        private static IBookDataServices iBookDataServices;

        /// <summary>
        /// Book validator.
        /// </summary>
        private static BookValidator validator = new BookValidator();

        /// <summary>
        /// IConstantsDataServices.
        /// </summary>
        private static IConstantsDataServices iConstantsDataServices;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="bookDataServices">bookDataServices.</param>
        /// <param name="constantsDataServices">constantsDataServices.</param>
        public BookServicesImplementation(ILog log, IBookDataServices bookDataServices, IConstantsDataServices constantsDataServices)
        {
            iLog = log;
            iBookDataServices = bookDataServices;
            iConstantsDataServices = constantsDataServices;
        }

        /// <summary>
        /// Adds the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void AddBook(Book book)
        {
            ValidationResult result = validator.Validate(book);
            if (result.IsValid)
            {
                var constants = iConstantsDataServices.GetLatestConstants();
                if (book.Domains.Count <= constants.DOM)
                {
                    iBookDataServices.AddBook(book);
                    iLog.Info($"The book with id {book.Id} has been added.");
                }
                else
                {
                    iLog.Error($"Book with title {book.Title} can't be added because it has too many domains.", new Exception($"Book with title {book.Title} can't be added because it has too many domains."));
                    throw new Exception($"Book with title {book.Title} can't be added because it has too many domains.");
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add book because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void DeleteBook(Book book)
        {
            try
            {
                iBookDataServices.DeleteBook(book);
                iLog.Info($"The book with id {book.Id} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns>All books.</returns>
        public IList<Book> GetAllBooks()
        {
            return iBookDataServices.GetAllBooks();
        }

        /// <summary>
        /// Updates the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void UpdateBook(Book book)
        {
            ValidationResult result = validator.Validate(book);
            if (result.IsValid)
            {
                var constants = iConstantsDataServices.GetLatestConstants();
                if (book.Domains.Count <= constants.DOM)
                {
                    try
                    {
                        iBookDataServices.UpdateBook(book);
                        iLog.Info($"The book with id {book.Id} has been upgraded.");
                    }
                    catch (Exception exception)
                    {
                        iLog.Error(exception.Message, new Exception(exception.Message));
                        throw new Exception(exception.Message);
                    }
                }
                else
                {
                    iLog.Error($"Book with title {book.Title} can't be updated because it has too many domains.", new Exception($"Book with title {book.Title} can't be added because it has too many domains."));
                    throw new Exception($"Book with title {book.Title} can't be updated because it has too many domains.");
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update book because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the book by given id.
        /// </summary>
        /// <param name="id">The book id.</param>
        public void DeleteBookById(int id)
        {
            var book = this.GetBookById(id);
            this.DeleteBook(book);
        }

        /// <summary>
        /// Gets the book by given id.
        /// </summary>
        /// <param name="id">The book id.</param>
        /// <returns>The book or null if there is no book with given id.</returns>
        public Book GetBookById(int id)
        {
            var book = iBookDataServices.GetBookById(id);
            if (book != null)
            {
                iLog.Info($"The book with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The book with id {id} does not exist.");
            }

            return book;
        }
    }
}
