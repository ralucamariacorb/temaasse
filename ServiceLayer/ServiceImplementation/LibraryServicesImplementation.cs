﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LibraryServicesImplementation.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer.ServiceImplementation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataMapper;
    using DomainModel;
    using DomainModel.Validators;
    using FluentValidation.Results;
    using log4net;

    /// <summary>
    /// LibraryServicesImplementation class.
    /// </summary>
    public class LibraryServicesImplementation : ILibraryServices
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static ILog iLog;

        /// <summary>
        /// ILibraryDataServices.
        /// </summary>
        private static ILibraryDataServices iLibraryDataServices;

        /// <summary>
        /// Library validator.
        /// </summary>
        private static LibraryValidator validator = new LibraryValidator();

        /// <summary>
        /// Initializes a new instance of the <see cref="LibraryServicesImplementation"/> class.
        /// </summary>
        /// <param name="log">log.</param>
        /// <param name="libraryDataServices">libraryDataServices.</param>
        public LibraryServicesImplementation(ILog log, ILibraryDataServices libraryDataServices)
        {
            iLog = log;
            iLibraryDataServices = libraryDataServices;
        }

        /// <summary>
        /// Adds the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void AddLibrary(Library library)
        {
            ValidationResult result = validator.Validate(library);
            if (result.IsValid)
            {
                iLibraryDataServices.AddLibrary(library);
                iLog.Info($"The library with id {library.EditionId} has been added.");
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't add library because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void DeleteLibrary(Library library)
        {
            try
            {
                iLibraryDataServices.DeleteLibrary(library);
                iLog.Info($"The library with id {library.EditionId} has been deleted.");
            }
            catch (Exception exception)
            {
                iLog.Error(exception.Message, new Exception(exception.Message));
                throw new Exception(exception.Message);
            }
        }

        /// <summary>
        /// Gets all librarys.
        /// </summary>
        /// <returns>All librarys.</returns>
        public IList<Library> GetAllLibrarys()
        {
            return iLibraryDataServices.GetAllLibrarys();
        }

        /// <summary>
        /// Updates the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void UpdateLibrary(Library library)
        {
            ValidationResult result = validator.Validate(library);
            if (result.IsValid)
            {
                try
                {
                    iLibraryDataServices.UpdateLibrary(library);
                    iLog.Info($"The library with id {library.EditionId} has been upgraded.");
                }
                catch (Exception exception)
                {
                    iLog.Error(exception.Message, new Exception(exception.Message));
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                StringBuilder errors = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    errors.Append(error);
                }

                iLog.Error(errors.ToString(), new Exception("Can't update library because it's not valid."));
                throw new Exception(errors.ToString());
            }
        }

        /// <summary>
        /// Deletes the library by given id.
        /// </summary>
        /// <param name="id">The library id.</param>
        public void DeleteLibraryById(int id)
        {
            var library = this.GetLibraryById(id);
            this.DeleteLibrary(library);
        }

        /// <summary>
        /// Gets the library by given id.
        /// </summary>
        /// <param name="id">The library id.</param>
        /// <returns>The library or null if there is no library with given id.</returns>
        public Library GetLibraryById(int id)
        {
            var library = iLibraryDataServices.GetLibraryById(id);
            if (library != null)
            {
                iLog.Info($"The library with id {id} exists.");
            }
            else
            {
                iLog.Warn($"The library with id {id} does not exist.");
            }

            return library;
        }
    }
}