﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEditionReaderServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IEditionReaderServices interface.
    /// </summary>
    public interface IEditionReaderServices
    {
        /// <summary>
        /// Gets all editionReaders.
        /// </summary>
        /// <returns>All editionReaders.</returns>
        IList<EditionReader> GetAllEditionReaders();

        /// <summary>
        /// Adds the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        void AddEditionReader(EditionReader editionReader);

        /// <summary>
        /// Deletes the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        void DeleteEditionReader(EditionReader editionReader);

        /// <summary>
        /// Updates the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        void UpdateEditionReader(EditionReader editionReader);
    }
}