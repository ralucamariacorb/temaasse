﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ServiceLayer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using DataMapper;
    using DataMapper.MySqlDAO;
    using DomainModel;
    using log4net;
    using log4net.Config;
    using ServiceLayer.ServiceImplementation;

    /// <summary>
    /// Program class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Log variable.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static IAuthorServices authorServices;
        private static IDomainServices domainServices;
        private static IEditionServices editionServices;
        private static IBookServices bookServices;
        private static IConstantsServices constantsServices;
        private static ILibraryServices libraryServices;
        private static IReaderServices readerServices;
        private static IEditionReaderServices editionReaderServices;

        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class.
        /// </summary>
        /// <param name="iAuthorServices">AuthorServices.</param>
        /// <param name="iDomainServices">DomainServices.</param>
        /// <param name="iEditionServices">EditionServices.</param>
        /// <param name="iBookServices">BookServices.</param>
        /// <param name="iConstantsServices">ConstantsServices.</param>
        /// <param name="iLibraryServices">LibraryServices.</param>
        /// <param name="iReaderServices">ReaderServices.</param>
        /// <param name="iEditionReaderServices">EditionReaderServices.</param>
        public Program(
            IAuthorServices iAuthorServices,
            IDomainServices iDomainServices,
            IEditionServices iEditionServices,
            IBookServices iBookServices,
            IConstantsServices iConstantsServices,
            ILibraryServices iLibraryServices,
            IReaderServices iReaderServices,
            IEditionReaderServices iEditionReaderServices)
        {
            authorServices = iAuthorServices;
            domainServices = iDomainServices;
            editionServices = iEditionServices;
            bookServices = iBookServices;
            constantsServices = iConstantsServices;
            libraryServices = iLibraryServices;
            readerServices = iReaderServices;
            editionReaderServices = iEditionReaderServices;
        }

        /// <summary>
        /// Main method.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public static void Main()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            var context = new LibraryContext();

            // Deletes previous database
            context.Database.EnsureDeleted();
            Log.Info("Database deleted!");

            // Creates the database if not exists
            context.Database.EnsureCreated();
            Log.Info("Database created!");

            InsertDummyData();

            var book = bookServices.GetBookById(1);
            var domain1 = domainServices.GetDomainById(1);
            var domain3 = domainServices.GetDomainById(3);
            var domain2 = domainServices.GetDomainById(2);
            var domain5 = domainServices.GetDomainById(5);
            var domain7 = domainServices.GetDomainById(7);

            if (CanAddDomain(book, domain1))
            {
                book.Domains.Add(new BookDomain
                {
                    Domain = domain1,
                    Book = book,
                });
                bookServices.UpdateBook(book);
            }

            if (CanAddDomain(book, domain3))
            {
                book.Domains.Add(new BookDomain
                {
                    Domain = domain3,
                    Book = book,
                });
                bookServices.UpdateBook(book);
            }

            if (CanAddDomain(book, domain2))
            {
                book.Domains.Add(new BookDomain
                {
                    Domain = domain2,
                    Book = book,
                });
                bookServices.UpdateBook(book);
            }

            if (CanAddDomain(book, domain5))
            {
                book.Domains.Add(new BookDomain
                {
                    Domain = domain5,
                    Book = book,
                });
                bookServices.UpdateBook(book);
            }

            if (CanAddDomain(book, domain7))
            {
                book.Domains.Add(new BookDomain
                {
                    Domain = domain7,
                    Book = book,
                });
                bookServices.UpdateBook(book);
            }
        }

        /// <summary>
        /// Inserts dummy data into the database.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public static void InsertDummyData()
        {
            authorServices = new AuthorServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.AuthorDataServices);

            domainServices = new DomainServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.DomainDataServices);

            editionServices = new EditionServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.EditionDataServices);

            constantsServices = new ConstantsServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.ConstantsDataServices);

            bookServices = new BookServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.BookDataServices, DAOFactoryMethod.CurrentDAOFactory.ConstantsDataServices);

            libraryServices = new LibraryServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.LibraryDataServices);

            readerServices = new ReaderServicesImplementation(Log, DAOFactoryMethod.CurrentDAOFactory.ReaderDataServices);

            Constants constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };

            constantsServices.AddConstants(constants);
            Constants constants1 = new Constants
            {
                DOM = 6,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            constantsServices.AddConstants(constants1);

            // Adds a author
            var author1 = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };
            var author2 = new Author
            {
                Firstname = "Peter",
                Lastname = "Norvig",
            };

            authorServices.AddAuthor(author1);
            authorServices.AddAuthor(author2);

            var domain1 = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };
            var domain2 = new Domain()
            {
                Name = "Matematica",
                Parent = domain1,
                ParentId = domain1.Id,
            };
            var domain3 = new Domain()
            {
                Name = "Informatica",
                Parent = domain1,
                ParentId = domain1.Id,
            };
            var domain4 = new Domain()
            {
                Name = "Algoritmi",
                Parent = domain3,
                ParentId = domain3.Id,
            };
            var domain5 = new Domain()
            {
                Name = "Retele",
                Parent = domain3,
                ParentId = domain3.Id,
            };
            var domain6 = new Domain()
            {
                Name = "Algoritmica grafurilor",
                Parent = domain4,
                ParentId = domain4.Id,
            };
            var domain7 = new Domain()
            {
                Name = "Algoritmi cuantici",
                Parent = domain4,
                ParentId = domain4.Id,
            };

            domainServices.AddDomain(domain1);
            domainServices.AddDomain(domain2);
            domainServices.AddDomain(domain3);
            domainServices.AddDomain(domain4);
            domainServices.AddDomain(domain5);
            domainServices.AddDomain(domain6);
            domainServices.AddDomain(domain7);

            var allDomains = domainServices.GetAllDomains();

            var edition1 = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
            };
            var edition2 = new Edition()
            {
                Number = 4,
                Year = 2020,
                Publisher = "Pearson",
                NumberOfPages = 1136,
                Type = "Hardcover",
            };

            // Adds some books
            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    edition1,
                    edition2,
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author1.Id,
            });

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author2.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain3.Id,
                BookId = book.Id,
            });

            bookServices.AddBook(book);

            Library library = new Library
            {
                EditionId = edition1.Id,
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };

            libraryServices.AddLibrary(library);

            Reader reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
                JoinDate = DateTime.Now.AddDays(-1),
            };

            readerServices.AddReader(reader);

            var a = authorServices.GetAuthorById(1);
        }

        /// <summary>
        /// Verifies if domain can be added to this book.
        /// </summary>
        /// <param name="book">The book for which we check if we can add the domain.</param>
        /// <param name="domain">The domain we want to add.</param>
        /// <returns>Return if the domain can be added or not.</returns>
        public static bool CanAddDomain(Book book, Domain domain)
        {
            var domains = domainServices.GetAllDomains();

            Domain subdomain;

            // Check if domain it's not a parent for one of the existing domains
            foreach (var bookDomain in book.Domains)
            {
                subdomain = domains.FirstOrDefault(d => d.Id == bookDomain.DomainId);

                while (subdomain != null)
                {
                    if (subdomain.Id == domain.Id)
                    {
                        return false;
                    }

                    subdomain = domains.FirstOrDefault(d => d.Id == subdomain.ParentId);
                }
            }

            // Check if domain it's not a child for one of the existing domains
            subdomain = domain;

            while (subdomain != null)
            {
                foreach (var bookDomain in book.Domains)
                {
                    if (subdomain.Id == bookDomain.DomainId)
                    {
                        return false;
                    }
                }

                subdomain = domains.FirstOrDefault(d => d.Id == subdomain.ParentId);
            }

            return true;
        }

        /// <summary>
        /// Borrow editions.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void BorrowEditions(Reader reader, List<Edition> editions)
        {
            this.CheckNMCandPERCondition(reader, editions);
            this.CheckCCondition(reader, editions);
            this.CheckDandLCondition(reader, editions);
            this.CheckDELTACondition(reader, editions);
            if (!reader.IsLibraryStaff)
            {
                this.CheckNCZCondition(reader, editions);
            }
        }

        /// <summary>
        /// Checks if NMC and PER condition pass.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void CheckNMCandPERCondition(Reader reader, List<Edition> editions)
        {
            var constants = constantsServices.GetLatestConstants();

            if (reader.IsLibraryStaff)
            {
                constants.NMC *= 2;
                constants.PER /= 2;
            }

            var allEditionReaders = editionReaderServices.GetAllEditionReaders();

            int countPER = 0;
            foreach (var editionReader in allEditionReaders)
            {
                if (DateTime.Compare(editionReader.BorrowTime, DateTime.Now.AddDays(-constants.PER)) > 0 &&
                    editionReader.ReaderId == reader.Id)
                {
                    countPER++;
                }
            }

            if (countPER + editions.Count >= constants.NMC)
            {
                string exceptionMessage =
                    $"You can't borrow more than {constants.NMC} editions in {constants.PER} months.";
                Log.Error(exceptionMessage, new Exception(exceptionMessage));

                throw new Exception(exceptionMessage);
            }

            Log.Info("NMC and PER condition passed.");
        }

        /// <summary>
        /// Checks if C condition pass.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void CheckCCondition(Reader reader, List<Edition> editions)
        {
            var constants = constantsServices.GetLatestConstants();

            if (reader.IsLibraryStaff)
            {
                constants.C *= 2;
            }

            if (editions.Count > constants.C)
            {
                string exceptionMessage = $"You can't borrow {editions.Count} editions. You can borrow maximum {constants.C}.";
                throw new Exception(exceptionMessage);
            }

            if (editions.Count >= 3)
            {
                var domainAparitions = new Dictionary<string, int>();
                foreach (var edition in editions)
                {
                    var book = bookServices.GetBookById(edition.Book.Id);
                    foreach (var domain in book.Domains)
                    {
                        var d = domainServices.GetDomainById(domain.DomainId);
                        while (d.Parent != null)
                        {
                            d = d.Parent;
                        }

                        if (domainAparitions.ContainsKey(d.Name))
                        {
                            domainAparitions[d.Name] = domainAparitions[d.Name] + 1;
                        }
                        else
                        {
                            domainAparitions.Add(d.Name, 1);
                        }
                    }
                }

                if (domainAparitions.Count < 2)
                {
                    string exceptionMessage = "You need to have editions from at least 2 domains.";
                    throw new Exception(exceptionMessage);
                }
            }
        }

        /// <summary>
        /// Checks if D and L condition pass.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void CheckDandLCondition(Reader reader, List<Edition> editions)
        {
            var allEditionReaders = editionReaderServices.GetAllEditionReaders();
            var constants = constantsServices.GetLatestConstants();

            if (reader.IsLibraryStaff)
            {
                constants.D *= 2;
            }

            var domainAparitions = new Dictionary<string, int>();
            foreach (var editionReader in allEditionReaders)
            {
                if (DateTime.Compare(editionReader.BorrowTime, DateTime.Now.AddMonths(-constants.L)) > 0 &&
                    editionReader.ReaderId == reader.Id)
                {
                    var edition = editionServices.GetEditionById(editionReader.EditionId);
                    var book = bookServices.GetBookById(edition.Book.Id);
                    foreach (var domain in book.Domains)
                    {
                        var d = domainServices.GetDomainById(domain.DomainId);
                        while (d.Parent != null)
                        {
                            d = d.Parent;
                        }

                        if (domainAparitions.ContainsKey(d.Name))
                        {
                            domainAparitions[d.Name] = domainAparitions[d.Name] + 1;
                        }
                        else
                        {
                            domainAparitions.Add(d.Name, 1);
                        }
                    }
                }
            }

            foreach (var ed in editions)
            {
                var bookEd = bookServices.GetBookById(ed.Book.Id);
                foreach (var domain in bookEd.Domains)
                {
                    var d = domainServices.GetDomainById(domain.DomainId);
                    while (d.Parent != null)
                    {
                        d = d.Parent;
                    }

                    if (domainAparitions.ContainsKey(d.Name))
                    {
                        domainAparitions[d.Name] = domainAparitions[d.Name] + 1;
                    }
                    else
                    {
                        domainAparitions.Add(d.Name, 1);
                    }
                }
            }

            foreach (KeyValuePair<string, int> domainAparition in domainAparitions)
            {
                if (domainAparition.Value > constants.D)
                {
                    var exceptionMessage = $"You can't borrow more than {constants.D} editions from the same domain in the last {constants.L} months.";
                    Log.Error(exceptionMessage, new Exception(exceptionMessage));

                    throw new Exception(exceptionMessage);
                }
            }

            Log.Info("D and L condition passed.");
        }

        /// <summary>
        /// Checks if DELTA condition pass.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void CheckDELTACondition(Reader reader, List<Edition> editions)
        {
            var allEditionReaders = editionReaderServices.GetAllEditionReaders();
            var constants = constantsServices.GetLatestConstants();

            if (reader.IsLibraryStaff)
            {
                constants.DELTA /= 2;
            }

            var domainAparitions = new Dictionary<int, DateTime>();
            foreach (var editionReader in allEditionReaders)
            {
                if (editionReader.ReaderId == reader.Id)
                {
                    if (domainAparitions.ContainsKey(editionReader.EditionId))
                    {
                        domainAparitions[editionReader.EditionId] = editionReader.BorrowTime;
                    }
                    else
                    {
                        domainAparitions.Add(editionReader.EditionId, editionReader.BorrowTime);
                    }
                }
            }

            foreach (var edition in editions)
            {
                if (domainAparitions.ContainsKey(edition.Id))
                {
                    if (DateTime.Compare(domainAparitions[edition.Id].AddDays(constants.DELTA), DateTime.Now) > 0)
                    {
                        string exceptionMessage = $"You can't borrow edition with id {edition.Id} now. {constants.DELTA} days didn't passed since last time you borrowed this edition.";
                        Log.Error(exceptionMessage, new Exception(exceptionMessage));

                        throw new Exception(exceptionMessage);
                    }

                    Log.Info("DELTA condition passed.");
                }
            }
        }

        /// <summary>
        /// Checks if NCZ condition pass.
        /// </summary>
        /// <param name="reader">The reader which wants to borrow.</param>
        /// <param name="editions">Editions to be borrowed.</param>
        public void CheckNCZCondition(Reader reader, List<Edition> editions)
        {
            var allEditionReaders = editionReaderServices.GetAllEditionReaders();
            var constants = constantsServices.GetLatestConstants();

            var countEditionsBorrowedToday = 0;

            foreach (var editionReader in allEditionReaders)
            {
                if (DateTime.Compare(editionReader.BorrowTime.Date, DateTime.Now.Date) == 0 &&
                    editionReader.ReaderId == reader.Id)
                {
                    countEditionsBorrowedToday++;
                }
            }

            if (countEditionsBorrowedToday + editions.Count > constants.NCZ)
            {
                string exceptionMessage = $"You can't borrow more than {constants.NCZ} editions in one day.";
                Log.Error(exceptionMessage, new Exception(exceptionMessage));

                throw new Exception(exceptionMessage);
            }

            Log.Info("NCZ condition passed.");
        }
    }
}
