﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    public class AuthorTests
    {
        private AuthorValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new AuthorValidator();
        }

        [Test]
        public void ValidAuthorTest()
        {
            var model = new Author
            {
                Id = 1,
                Firstname = "AuthorFirstname",
                Lastname = "AuthorLastname"
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.Firstname);
            result.ShouldNotHaveValidationErrorFor(m => m.Lastname);
            result.ShouldNotHaveValidationErrorFor(m => m.Books);
        }
        
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyFirstnameTest(string firstname)
        {
            var model = new Author { Firstname = firstname };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Firstname);
        }

        [Test]
        public void ErrorTooShortFirstnameTest()
        {
            var model = new Author { Firstname = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Firstname);
        }

        [Test]
        public void ErrorTooLongFirstnameTest()
        {
            var model = new Author { Firstname = new string('a', 21) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Firstname);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyLastnameTest(string lastname)
        {
            var model = new Author { Lastname = lastname };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Lastname);
        }

        [Test]
        public void ErrorTooShortLastnameTest()
        {
            var model = new Author { Lastname = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Lastname);
        }

        [Test]
        public void ErrorTooLongLastnameTest()
        {
            var model = new Author { Lastname = new string('a', 21) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Lastname);
        }
    }
}
