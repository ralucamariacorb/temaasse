﻿using System.Collections.Generic;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    public class BookTests
    {
        private BookValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new BookValidator();
        }

        [Test]
        public void ValidBookTest()
        {
            var model = new Book
            {
                Id = 1,
                Title = "BookTitle",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 1,
                        Year = 2010,
                        Publisher = "EditionPublisher",
                        NumberOfPages = 1152,
                        Type = "Hardcover"
                    }
                },
            };

            model.Authors = new List<BookAuthor>
            {
                new BookAuthor()
                {
                    Author = new Author
                    {
                        Id = 1,
                        Firstname = "AuthorFirstname",
                        Lastname = "AuthorLastname"
                    },
                    AuthorId = 1,
                    BookId = 1,
                    Book = model
                }
            };

            model.Domains = new List<BookDomain>
            {
                new BookDomain()
                {
                    Domain = new Domain
                    {
                        Id = 1,
                        Name = "DomainName",
                        Parent = null
                    },
                    DomainId = 1,
                    BookId = 1,
                    Book = model
                }
            };


            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.Title);
            result.ShouldNotHaveValidationErrorFor(m => m.Domains);
            result.ShouldNotHaveValidationErrorFor(m => m.Authors);
            result.ShouldNotHaveValidationErrorFor(m => m.Editions);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyTitleTest(string title)
        {
            var model = new Book { Title = title };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Title);
        }

        [Test]
        public void ErrorTooShortTitleTest()
        {
            var model = new Book { Title = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Title);
        }

        [Test]
        public void ErrorTooLongTitleTest()
        {
            var model = new Book { Title = new string('a', 51) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Title);
        }

        [Test]
        public void ErrorNullDomainsTest()
        {
            var model = new Book { Domains = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Domains);
        }

        [Test]
        public void ErrorEmptyDomainsTest()
        {
            var model = new Book { Domains = new List<BookDomain>() };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Domains);
        }

        [Test]
        public void ErrorNullAuthorsTest()
        {
            var model = new Book { Authors = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Authors);
        }

        [Test]
        public void ErrorEmptyAuthorsTest()
        {
            var model = new Book { Authors = new List<BookAuthor>() };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Authors);
        }

        [Test]
        public void ErrorNullEditionsTest()
        {
            var model = new Book { Editions = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Editions);
        }

        [Test]
        public void ErrorEmptyEditionsTest()
        {
            var model = new Book { Editions = new List<Edition>() };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Editions);
        }
    }
}
