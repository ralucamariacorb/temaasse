﻿using System;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class ConstantsTests
    {
        private ConstantsValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new ConstantsValidator();
        }

        [Test]
        public void ValidConstantsTest()
        {
            var model = new Constants
            {
                Id = 1,
                DOM = 3,
                NMC = 8,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 4
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.DOM);
            result.ShouldNotHaveValidationErrorFor(m => m.NMC);
            result.ShouldNotHaveValidationErrorFor(m => m.PER);
            result.ShouldNotHaveValidationErrorFor(m => m.C);
            result.ShouldNotHaveValidationErrorFor(m => m.D);
            result.ShouldNotHaveValidationErrorFor(m => m.L);
            result.ShouldNotHaveValidationErrorFor(m => m.LIM);
            result.ShouldNotHaveValidationErrorFor(m => m.DELTA);
            result.ShouldNotHaveValidationErrorFor(m => m.NCZ);
        }

        [Test]
        public void ErrorInvalidDOMTest()
        {
            var model = new Constants { DOM = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.DOM);
        }

        [Test]
        public void ErrorInvalidNMCTest()
        {
            var model = new Constants { NMC = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NMC);
        }

        [Test]
        public void ErrorInvalidPERTest()
        {
            var model = new Constants { PER = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.PER);
        }

        [TestCase(0, 0)]
        [TestCase(5, 4)]
        public void ErrorInvalidCTest(int c, int ncz)
        {
            var model = new Constants { C = c, NCZ = ncz };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.C);
        }

        [TestCase(0, 0)]
        [TestCase(5, 4)]
        public void ErrorInvalidDTest(int d, int c)
        {
            var model = new Constants { D = d, C = c };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.D);
        }

        [Test]
        public void ErrorInvalidLTest()
        {
            var model = new Constants { L = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.L);
        }

        [Test]
        public void ErrorInvalidLIMTest()
        {
            var model = new Constants { LIM = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.LIM);
        }

        [Test]
        public void ErrorInvalidDELTATest()
        {
            var model = new Constants { DELTA = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.DELTA);
        }

        [TestCase(0, 0)]
        [TestCase(5, 4)]
        public void ErrorInvalidNCZTest(int ncz, int nmc)
        {
            var model = new Constants { NCZ = ncz, NMC = nmc };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NCZ);
        }
    }
}
