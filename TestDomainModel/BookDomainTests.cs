﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class BookDomainTests
    {
        private BookDomainValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new BookDomainValidator();
        }

        [Test]
        public void ValidBookDomainTest()
        {
            var model = new BookDomain
            {
                BookId = 1,
                Book = new Book(),
                DomainId = 1,
                Domain = new Domain()
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.BookId);
            result.ShouldNotHaveValidationErrorFor(m => m.Book);
            result.ShouldNotHaveValidationErrorFor(m => m.DomainId);
            result.ShouldNotHaveValidationErrorFor(m => m.Domain);
        }

        [Test]
        public void ErrorInvalidBookIdTest()
        {
            var model = new BookDomain { BookId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.BookId);
        }

        [Test]
        public void ErrorInvalidDomainIdTest()
        {
            var model = new BookDomain { DomainId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.DomainId);
        }

        [Test]
        public void ErrorEmptyBookTest()
        {
            var model = new BookDomain { Book = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Book);
        }

        [Test]
        public void ErrorEmptyDomainTest()
        {
            var model = new BookDomain { Domain = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Domain);
        }
    }
}
