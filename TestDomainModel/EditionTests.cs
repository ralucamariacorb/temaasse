﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    public class EditionTests
    {
        private EditionValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new EditionValidator();
        }

        [Test]
        public void ValidEditionTest()
        {
            var model = new Edition
            {
                Id = 1,
                Number = 1,
                Year = 2010,
                Publisher = "EditionPublisher",
                NumberOfPages = 1152,
                Type = "Hardcover",
                Book = new Book(),
                Readers = null
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.Number);
            result.ShouldNotHaveValidationErrorFor(m => m.Year);
            result.ShouldNotHaveValidationErrorFor(m => m.Publisher);
            result.ShouldNotHaveValidationErrorFor(m => m.NumberOfPages);
            result.ShouldNotHaveValidationErrorFor(m => m.Type);
            result.ShouldNotHaveValidationErrorFor(m => m.Book);
            result.ShouldNotHaveValidationErrorFor(m => m.Readers);
        }

        [Test]
        public void ErrorInvalidNumberTest()
        {
            var model = new Edition { Number = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Number);
        }

        [TestCase(1799)]
        [TestCase(2022)]
        public void ErrorInvalidYearTest(int year)
        {
            var model = new Edition { Year = year };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Year);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyPublisherTest(string publisher)
        {
            var model = new Edition { Publisher = publisher };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Publisher);
        }

        [Test]
        public void ErrorTooShortPublisherTest()
        {
            var model = new Edition { Publisher = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Publisher);
        }

        [Test]
        public void ErrorTooLongPublisherTest()
        {
            var model = new Edition { Publisher = new string('a', 21) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Publisher);
        }

        [Test]
        public void ErrorInvalidNumberOfPagesTest()
        {
            var model = new Edition { NumberOfPages = 5 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NumberOfPages);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyTypeTest(string type)
        {
            var model = new Edition { Type = type };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Type);
        }

        [Test]
        public void ErrorTooShortTypeTest()
        {
            var model = new Edition { Type = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Type);
        }

        [Test]
        public void ErrorTooLongTypeTest()
        {
            var model = new Edition { Type = new string('a', 21) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Type);
        }

        [Test]
        public void ErrorEmptyBookTest()
        {
            var model = new Edition { Book = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Book);
        }
    }
}
