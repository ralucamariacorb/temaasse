﻿using System;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class EditionReaderTests
    {
        private EditionReaderValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new EditionReaderValidator();
        }

        [Test]
        public void ValidEditionReaderTest()
        {
            var model = new EditionReader
            {
                EditionId = 1,
                Edition = new Edition(),
                ReaderId = 1,
                Reader = new Reader(),
                BorrowTime = DateTime.Now,
                ShouldReturnTime = DateTime.Now.AddDays(14),
                ReturnTime = DateTime.Now.AddDays(14)
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.EditionId);
            result.ShouldNotHaveValidationErrorFor(m => m.Edition);
            result.ShouldNotHaveValidationErrorFor(m => m.ReaderId);
            result.ShouldNotHaveValidationErrorFor(m => m.Reader);
            result.ShouldNotHaveValidationErrorFor(m => m.BorrowTime);
            result.ShouldNotHaveValidationErrorFor(m => m.ShouldReturnTime);
        }

        [Test]
        public void ErrorInvalidEditionIdTest()
        {
            var model = new EditionReader { EditionId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.EditionId);
        }

        [Test]
        public void ErrorInvalidReaderIdTest()
        {
            var model = new EditionReader { ReaderId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.ReaderId);
        }

        [Test]
        public void ErrorEmptyEditionTest()
        {
            var model = new EditionReader { Edition = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Edition);
        }

        [Test]
        public void ErrorEmptyBorrowTimeTest()
        {
            var model = new EditionReader();
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.BorrowTime);
        }

        [Test]
        public void ErrorEmptyShouldReturnTimeTest()
        {
            var model = new EditionReader();
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.ShouldReturnTime);
        }

        [Test]
        public void ErrorInvalidShouldReturnTimeTest()
        {
            var model = new EditionReader
            {
                BorrowTime = new DateTime(2021, 1, 1),
                ShouldReturnTime = new DateTime(2020, 1, 1)
            };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.ShouldReturnTime);
        }

        [Test]
        public void ErrorInvalidReturnTimeTest()
        {
            var model = new EditionReader
            {
                ReturnTime = new DateTime(2020, 1, 1),
                BorrowTime = new DateTime(2021, 1, 1)
            };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.ReturnTime);
        }
    }
}
