﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class DomainTests
    {
        private DomainValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new DomainValidator();
        }

        [Test]
        public void ValidDomainTest()
        {
            var model = new Domain
            {
                Id = 1,
                Name = "DomainName",
                Parent = null,
                Books = null
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.Name);
            result.ShouldNotHaveValidationErrorFor(m => m.Parent);
            result.ShouldNotHaveValidationErrorFor(m => m.Books);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyNameTest(string name)
        {
            var model = new Domain { Name = name };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Name);
        }

        [Test]
        public void ErrorTooShortNameTest()
        {
            var model = new Domain { Name = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Name);
        }

        [Test]
        public void ErrorTooLongNameTest()
        {
            var model = new Domain { Name = new string('a', 51) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Name);
        }
    }
}
