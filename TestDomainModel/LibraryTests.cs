﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class LibraryTests
    {
        private LibraryValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new LibraryValidator();
        }

        [Test]
        public void ValidLibraryTest()
        {
            Edition edition = new Edition();

            var model = new Library
            {
                EditionId = edition.Id,
                Edition = edition,
                TotalNumberOfCopies = 10,
                NumberOfCopiesReadingRoom = 5,
                NumberOfBorrowedCopies = 5
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Edition);
            result.ShouldNotHaveValidationErrorFor(m => m.TotalNumberOfCopies);
            result.ShouldNotHaveValidationErrorFor(m => m.NumberOfCopiesReadingRoom);
            result.ShouldNotHaveValidationErrorFor(m => m.NumberOfBorrowedCopies);
        }

        [Test]
        public void ErrorInvalidEditionIdTest()
        {
            var model = new Library { EditionId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.EditionId);
        }

        [Test]
        public void ErrorInvalidTotalNumberOfCopiesTest()
        {
            Edition edition = new Edition();

            var model = new Library
            {
                EditionId = edition.Id,
                Edition = edition,
                TotalNumberOfCopies = -1,
                NumberOfCopiesReadingRoom = 0,
                NumberOfBorrowedCopies = 0
            };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.TotalNumberOfCopies);
        }

        [Test]
        public void ErrorInvalidNumberOfCopiesReadingRoomTest()
        {
            Edition edition = new Edition();

            var model = new Library
            {
                EditionId = edition.Id,
                Edition = edition,
                TotalNumberOfCopies = 2,
                NumberOfCopiesReadingRoom = 3,
                NumberOfBorrowedCopies = 0
            };

            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NumberOfCopiesReadingRoom);
        }

        [Test]
        public void ErrorInvalidNumberOfBorrowedCopiesTest()
        {
            Edition edition = new Edition();

            var model = new Library
            {
                EditionId = edition.Id,
                Edition = edition,
                TotalNumberOfCopies = 2,
                NumberOfCopiesReadingRoom = 1,
                NumberOfBorrowedCopies = 3
            };

            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NumberOfBorrowedCopies);
        }

        //[TestCase(50, 20, 10, true)]
        //[TestCase(30, 20, 10, false)]
        //public void ShouldSetCanBeBorrowedTest(int totalNumberOfCopies, int numberOfCopiesReadingRoom, int numberOfBorrowedCopies, bool expectedResult)
        //{
        //    Edition edition = new Edition();

        //    var model = new Library
        //    {
        //        EditionId = edition.Id,
        //        Edition = edition,
        //        TotalNumberOfCopies = totalNumberOfCopies,
        //        NumberOfCopiesReadingRoom = numberOfCopiesReadingRoom,
        //        NumberOfBorrowedCopies = numberOfBorrowedCopies
        //    };
        //    Assert.AreEqual(expectedResult, model.CanBeBorrowed());
        //}
    }
}
