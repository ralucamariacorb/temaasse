﻿using System;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class ReaderTests
    {
        private ReaderValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new ReaderValidator();
        }

        [TestCase("email@gmail.com", "1234567890")]
        [TestCase("email@gmail.com", "")]
        [TestCase("email@gmail.com", " ")]
        [TestCase("email@gmail.com", null)]
        [TestCase("", "1234567890")]
        [TestCase(" ", "1234567890")]
        [TestCase(null, "1234567890")]
        public void ValidReaderTest(string email, string phoneNumber)
        {
            var model = new Reader
            {
                Id = 1,
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = email,
                PhoneNumber = phoneNumber,
                NumberOfExtensions = 0,
                BorrowedEditions = null,
                IsLibraryStaff = false,
                JoinDate = new DateTime(2021, 1, 1)
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.Id);
            result.ShouldNotHaveValidationErrorFor(m => m.Lastname);
            result.ShouldNotHaveValidationErrorFor(m => m.Firstname);
            result.ShouldNotHaveValidationErrorFor(m => m.Address);
            result.ShouldNotHaveValidationErrorFor(m => m.Email);
            result.ShouldNotHaveValidationErrorFor(m => m.PhoneNumber);
            result.ShouldNotHaveValidationErrorFor(m => m.NumberOfExtensions);
            result.ShouldNotHaveValidationErrorFor(m => m.BorrowedEditions);
            result.ShouldNotHaveValidationErrorFor(m => m.IsLibraryStaff);
            result.ShouldNotHaveValidationErrorFor(m => m.JoinDate);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyLastnameTest(string lastname)
        {
            var model = new Reader { Lastname = lastname };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Lastname);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyFirstnameTest(string firstname)
        {
            var model = new Reader { Firstname = firstname };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Firstname);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void ErrorEmptyAddressTest(string address)
        {
            var model = new Reader { Address = address };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Address);
        }

        [TestCase(null, null)]
        [TestCase(null, "")]
        [TestCase(null, " ")]
        [TestCase("", null)]
        [TestCase("", "")]
        [TestCase("", " ")]
        [TestCase(" ", null)]
        [TestCase(" ", "")]
        [TestCase(" ", " ")]
        public void ErrorEmptyEmailAndPhoneNumberTest(string email, string phoneNumber)
        {
            var model = new Reader { Email = email, PhoneNumber = phoneNumber };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Email);
            result.ShouldHaveValidationErrorFor(m => m.PhoneNumber);
        }

        [Test]
        public void ErrorTooShortEmailTest()
        {
            var model = new Reader { Email = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Email);
        }

        [Test]
        public void ErrorTooLongEmailTest()
        {
            var model = new Reader { Email = new string('a', 101) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Email);
        }

        [Test]
        public void ErrorTooShortPhoneNumberTest()
        {
            var model = new Reader { PhoneNumber = "a" };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.PhoneNumber);
        }

        [Test]
        public void ErrorTooLongPhoneNumberTest()
        {
            var model = new Reader { PhoneNumber = new string('a', 11) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.PhoneNumber);
        }


        [Test]
        public void ErrorInvalidNumberOfExtensionsTest()
        {
            var model = new Reader { NumberOfExtensions = -1 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.NumberOfExtensions);
        }

        [Test]
        public void ErrorInvalidJoinDateTest()
        {
            var model = new Reader { JoinDate = DateTime.Now.Add(new TimeSpan(1, 0, 0, 0)) };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.JoinDate);
        }
    }
}
