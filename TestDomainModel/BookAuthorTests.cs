﻿using DomainModel;
using DomainModel.Validators;
using FluentValidation.TestHelper;
using NUnit.Framework;

namespace TestDomainModel
{
    class BookAuthorTests
    {
        private BookAuthorValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new BookAuthorValidator();
        }

        [Test]
        public void ValidBookAuthorTest()
        {
            var model = new BookAuthor
            {
                BookId = 1,
                Book = new Book(),
                AuthorId = 1,
                Author = new Author()
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(m => m.BookId);
            result.ShouldNotHaveValidationErrorFor(m => m.Book);
            result.ShouldNotHaveValidationErrorFor(m => m.AuthorId);
            result.ShouldNotHaveValidationErrorFor(m => m.Author);
        }

        [Test]
        public void ErrorInvalidBookIdTest()
        {
            var model = new BookAuthor { BookId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.BookId);
        }

        [Test]
        public void ErrorInvalidAuthorIdTest()
        {
            var model = new BookAuthor { AuthorId = 0 };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.AuthorId);
        }

        [Test]
        public void ErrorEmptyBookTest()
        {
            var model = new BookAuthor { Book = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Book);
        }

        [Test]
        public void ErrorEmptyAuthorTest()
        {
            var model = new BookAuthor { Author = null };
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(m => m.Author);
        }
    }
}
