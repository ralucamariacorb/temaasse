﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookDomain.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// BookDomain class.
    /// </summary>
    public class BookDomain
    {
        /// <summary>
        /// Gets or sets book id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int BookId { get; set; }

        /// <summary>
        /// Gets or sets book.
        /// </summary>
        [Required]
        public Book Book { get; set; }

        /// <summary>
        /// Gets or sets domain id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int DomainId { get; set; }

        /// <summary>
        /// Gets or sets domain.
        /// </summary>
        [Required]
        public Domain Domain { get; set; }
    }
}
