﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Constants class.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Gets or sets constants id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of domains for a book.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int DOM { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of books which a reader can borrow in a period PER.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int NMC { get; set; }

        /// <summary>
        /// Gets or sets the period in which a reader can borrow maximum NMC books.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int PER { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of books a reader can borrow once.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int C { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of books a reader can borrow from the same domain.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int D { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of months in which a reader can borrow maximum D books from the same domain.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int L { get; set; }

        /// <summary>
        /// Gets or sets the limit of the number of extensions in the last 3 months.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int LIM { get; set; }

        /// <summary>
        /// Gets or sets the period in which a user can't borrow a book if he already borrowed it.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int DELTA { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of books a reader can borrow in one day.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int NCZ { get; set; }
    }
}
