﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookAuthor.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// BookAuthor class.
    /// </summary>
    public class BookAuthor
    {
        /// <summary>
        /// Gets or sets book id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int BookId { get; set; }

        /// <summary>
        /// Gets or sets book.
        /// </summary>
        [Required]
        public Book Book { get; set; }

        /// <summary>
        /// Gets or sets author id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int AuthorId { get; set; }

        /// <summary>
        /// Gets or sets author.
        /// </summary>
        [Required]
        public Author Author { get; set; }
    }
}
