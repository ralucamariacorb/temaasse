﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Book.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Book class.
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        public Book()
        {
            this.Domains = new HashSet<BookDomain>();
            this.Authors = new HashSet<BookAuthor>();
            this.Editions = new HashSet<Edition>();
        }

        /// <summary>
        /// Gets or sets book id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets book name.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets book domains.
        /// </summary>
        [Required]
        public ICollection<BookDomain> Domains { get; set; }

        /// <summary>
        /// Gets or sets book authors.
        /// </summary>
        [Required]
        public ICollection<BookAuthor> Authors { get; set; }

        /// <summary>
        /// Gets or sets book editions.
        /// </summary>
        public ICollection<Edition> Editions { get; set; }
    }
}
