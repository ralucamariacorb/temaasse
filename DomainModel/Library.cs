﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Library.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Library class.
    /// </summary>
    public class Library
    {
        /// <summary>
        /// Gets or sets book id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EditionId { get; set; }

        /// <summary>
        /// Gets or sets book.
        /// </summary>
        [Required]
        public Edition Edition { get; set; }

        /// <summary>
        /// Gets or sets the total number of copies for a book.
        /// </summary>
        [Required]
        public int TotalNumberOfCopies { get; set; }

        /// <summary>
        /// Gets or sets the number of copies for reading room for a book.
        /// </summary>
        [Required]
        public int NumberOfCopiesReadingRoom { get; set; }

        /// <summary>
        /// Gets or sets the number of borrowed copies for a book.
        /// </summary>
        [Required]
        public int NumberOfBorrowedCopies { get; set; }
    }
}
