﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Edition.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Edition class.
    /// </summary>
    public class Edition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Edition"/> class.
        /// </summary>
        public Edition()
        {
            this.Readers = new HashSet<EditionReader>();
        }

        /// <summary>
        /// Gets or sets edition id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the number for this edition.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the year for this edition.
        /// </summary>
        [Required]
        [Range(1800, 2021)]
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets the publisher for this edition.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets number of pages for this edition.
        /// </summary>
        [Required]
        [Range(5, int.MaxValue)]
        public int NumberOfPages { get; set; }

        /// <summary>
        /// Gets or sets the type for this edition.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets book id.
        /// </summary>
        public int? BookId { get; set; } = null;

        /// <summary>
        /// Gets or sets the book for this edition.
        /// </summary>
        public virtual Book Book { get; set; }

        /// <summary>
        /// Gets or sets library for this edition.
        /// </summary>
        public Library Library { get; set; }

        /// <summary>
        /// Gets or sets readers for this edition.
        /// </summary>
        public ICollection<EditionReader> Readers { get; set; }
    }
}
