﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantsValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Constants validator class.
    /// </summary>
    public class ConstantsValidator : AbstractValidator<Constants>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConstantsValidator"/> class.
        /// </summary>
        public ConstantsValidator()
        {
            this.RuleFor(x => x.DOM).GreaterThan(0);
            this.RuleFor(x => x.NMC).GreaterThan(0);
            this.RuleFor(x => x.PER).GreaterThan(0);
            this.RuleFor(x => x.C).GreaterThan(0).LessThanOrEqualTo(m => m.NCZ);
            this.RuleFor(x => x.D).GreaterThan(0).LessThanOrEqualTo(m => m.C);
            this.RuleFor(x => x.L).GreaterThan(0);
            this.RuleFor(x => x.LIM).GreaterThan(0);
            this.RuleFor(x => x.DELTA).GreaterThan(0);
            this.RuleFor(x => x.NCZ).GreaterThan(0).LessThanOrEqualTo(m => m.NMC);
        }
    }
}
