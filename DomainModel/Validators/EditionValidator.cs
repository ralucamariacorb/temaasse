﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditionValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Edition validator class.
    /// </summary>
    public class EditionValidator : AbstractValidator<Edition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditionValidator"/> class.
        /// </summary>
        public EditionValidator()
        {
            this.RuleFor(x => x.Number).GreaterThan(0);
            this.RuleFor(x => x.Year).InclusiveBetween(1800, 2021);
            this.RuleFor(x => x.Publisher).NotEmpty().Length(2, 20);
            this.RuleFor(x => x.NumberOfPages).GreaterThan(5);
            this.RuleFor(x => x.Type).NotEmpty().Length(2, 20);
            this.RuleFor(x => x.Book).NotEmpty();
        }
    }
}
