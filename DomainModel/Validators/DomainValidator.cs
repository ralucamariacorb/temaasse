﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DomainValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Domain validator class.
    /// </summary>
    public class DomainValidator : AbstractValidator<Domain>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainValidator"/> class.
        /// </summary>
        public DomainValidator()
        {
            this.RuleFor(x => x.Name).NotEmpty().Length(2, 50);
        }
    }
}
