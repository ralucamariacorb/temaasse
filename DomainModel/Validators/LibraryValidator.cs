﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LibraryValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Library validator class.
    /// </summary>
    public class LibraryValidator : AbstractValidator<Library>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LibraryValidator"/> class.
        /// </summary>
        public LibraryValidator()
        {
            this.RuleFor(x => x.EditionId).GreaterThan(0);
            this.RuleFor(x => x.TotalNumberOfCopies).GreaterThanOrEqualTo(0);
            this.RuleFor(x => x.NumberOfCopiesReadingRoom).LessThanOrEqualTo(model => model.TotalNumberOfCopies);
            this.RuleFor(x => x.NumberOfBorrowedCopies).GreaterThanOrEqualTo(0).LessThanOrEqualTo(model => model.TotalNumberOfCopies - model.NumberOfCopiesReadingRoom);
        }
    }
}
