﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookAuthorValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// BookAuthor validator class.
    /// </summary>
    public class BookAuthorValidator : AbstractValidator<BookAuthor>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookAuthorValidator"/> class.
        /// </summary>
        public BookAuthorValidator()
        {
            this.RuleFor(x => x.BookId).NotEmpty();
            this.RuleFor(x => x.Book).NotEmpty();
            this.RuleFor(x => x.AuthorId).NotEmpty();
            this.RuleFor(x => x.Author).NotEmpty();
        }
    }
}
