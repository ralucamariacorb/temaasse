﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReaderValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using System;
    using FluentValidation;

    /// <summary>
    /// Reader validator class.
    /// </summary>
    public class ReaderValidator : AbstractValidator<Reader>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderValidator"/> class.
        /// </summary>
        public ReaderValidator()
        {
            this.RuleFor(x => x.Lastname).NotEmpty().Length(2, 50);
            this.RuleFor(x => x.Firstname).NotEmpty().Length(2, 50);
            this.RuleFor(x => x.Address).NotEmpty().Length(2, 100);
            this.RuleFor(m => m.Email).EmailAddress().NotEmpty().Length(2, 100).When(m => string.IsNullOrWhiteSpace(m.PhoneNumber));
            this.RuleFor(m => m.PhoneNumber).NotEmpty().Length(2, 10).When(m => string.IsNullOrWhiteSpace(m.Email));
            this.RuleFor(m => m.NumberOfExtensions).GreaterThanOrEqualTo(0);
            this.RuleFor(m => m.JoinDate).LessThanOrEqualTo(DateTime.Now);
        }
    }
}
