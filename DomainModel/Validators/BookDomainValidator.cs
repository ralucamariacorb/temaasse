﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookDomainValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// BookDomain validator class.
    /// </summary>
    public class BookDomainValidator : AbstractValidator<BookDomain>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookDomainValidator"/> class.
        /// </summary>
        public BookDomainValidator()
        {
            this.RuleFor(x => x.BookId).GreaterThan(0);
            this.RuleFor(x => x.Book).NotEmpty();
            this.RuleFor(x => x.DomainId).GreaterThan(0);
            this.RuleFor(x => x.Domain).NotEmpty();
        }
    }
}
