﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditionReaderValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// EditionReader validator class.
    /// </summary>
    public class EditionReaderValidator : AbstractValidator<EditionReader>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditionReaderValidator"/> class.
        /// </summary>
        public EditionReaderValidator()
        {
            this.RuleFor(x => x.EditionId).GreaterThan(0);
            this.RuleFor(x => x.Edition).NotEmpty();
            this.RuleFor(x => x.ReaderId).GreaterThan(0);
            this.RuleFor(x => x.Reader).NotEmpty();
            this.RuleFor(x => x.BorrowTime).NotEmpty();
            this.RuleFor(x => x.ShouldReturnTime).NotEmpty().GreaterThanOrEqualTo(d => d.BorrowTime);
            this.RuleFor(x => x.ReturnTime).LessThanOrEqualTo(d => d.ShouldReturnTime);
        }
    }
}
