﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Book validator class.
    /// </summary>
    public class BookValidator : AbstractValidator<Book>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookValidator"/> class.
        /// </summary>
        public BookValidator()
        {
            this.RuleFor(x => x.Title).NotEmpty().Length(2, 50);
            this.RuleFor(x => x.Domains).NotEmpty();
            this.RuleFor(x => x.Authors).NotEmpty();
            this.RuleFor(x => x.Editions).NotEmpty();
        }
    }
}
