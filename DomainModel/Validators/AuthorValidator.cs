﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthorValidator.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel.Validators
{
    using FluentValidation;

    /// <summary>
    /// Author validator class.
    /// </summary>
    public class AuthorValidator : AbstractValidator<Author>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorValidator"/> class.
        /// </summary>
        public AuthorValidator()
        {
            this.RuleFor(x => x.Firstname).NotEmpty().Length(2, 20);
            this.RuleFor(x => x.Lastname).NotEmpty().Length(2, 20);
        }
    }
}
