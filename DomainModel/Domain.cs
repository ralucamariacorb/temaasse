﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Domain.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Domain class.
    /// </summary>
    public class Domain
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Domain"/> class.
        /// </summary>
        public Domain()
        {
            this.SubDomains = new HashSet<Domain>();
            this.Books = new HashSet<BookDomain>();
        }

        /// <summary>
        /// Gets or sets domain id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets domain name.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets domain parent id.
        /// </summary>
        public int? ParentId { get; set; } = null;

        /// <summary>
        /// Gets or sets domain parent.
        /// </summary>
        public virtual Domain Parent { get; set; }

        /// <summary>
        /// Gets or sets domain children.
        /// </summary>
        public virtual ICollection<Domain> SubDomains { get; set; }

        /// <summary>
        /// Gets or sets domain books.
        /// </summary>
        public ICollection<BookDomain> Books { get; set; }
    }
}
