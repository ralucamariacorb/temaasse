﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Author.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Author class.
    /// </summary>
    public class Author
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/> class.
        /// </summary>
        public Author()
        {
            this.Books = new HashSet<BookAuthor>();
        }

        /// <summary>
        /// Gets or sets author id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets author firstname.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets author lastname.
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Lastname { get; set; }

        /// <summary>
        /// Gets or sets author books.
        /// </summary>
        public ICollection<BookAuthor> Books { get; set; }
    }
}
