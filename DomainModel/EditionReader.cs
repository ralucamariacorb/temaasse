﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditionReader.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// EditionReader class.
    /// </summary>
    public class EditionReader
    {
        /// <summary>
        /// Gets or sets edition id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int EditionId { get; set; }

        /// <summary>
        /// Gets or sets edition.
        /// </summary>
        [Required]
        public Edition Edition { get; set; }

        /// <summary>
        /// Gets or sets reader id.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int ReaderId { get; set; }

        /// <summary>
        /// Gets or sets reader.
        /// </summary>
        [Required]
        public Reader Reader { get; set; }

        /// <summary>
        /// Gets or sets the time when a reader borrowed a edition.
        /// </summary>
        [Required]
        public DateTime BorrowTime { get; set; }

        /// <summary>
        /// Gets or sets the time when a reader should return a edition.
        /// </summary>
        [Required]
        public DateTime ShouldReturnTime { get; set; }

        /// <summary>
        /// Gets or sets the time when a reader returned a edition.
        /// </summary>
        public DateTime? ReturnTime { get; set; }
    }
}
