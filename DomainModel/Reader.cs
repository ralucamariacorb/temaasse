﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Reader.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Reader class.
    /// </summary>
    public class Reader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Reader"/> class.
        /// </summary>
        public Reader()
        {
            this.JoinDate = DateTime.Now;
            this.BorrowedEditions = new HashSet<EditionReader>();
        }

        /// <summary>
        /// Gets or sets reader id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets reader lastname.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Lastname { get; set; }

        /// <summary>
        /// Gets or sets reader firstname.
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets reader address.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets reader email.
        /// </summary>
        [EmailAddress]
        [StringLength(100, MinimumLength = 0)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets reader phone number.
        /// </summary>
        [StringLength(10, MinimumLength = 0)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this reader is also part from the library staff.
        /// </summary>
        [Required]
        public bool IsLibraryStaff { get; set; }

        /// <summary>
        /// Gets or sets the editions borrowed by this reader.
        /// </summary>
        public ICollection<EditionReader> BorrowedEditions { get; set; }

        /// <summary>
        /// Gets or sets the number of extensions this reader had in last 3 months.
        /// </summary>
        [Required]
        public int NumberOfExtensions { get; set; }

        /// <summary>
        /// Gets or sets the date when the reader joined.
        /// </summary>
        [Required]
        public DateTime JoinDate { get; set; }
    }
}
