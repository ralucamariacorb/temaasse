﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class DomainServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private DomainServicesImplementation _domainServicesImplementation;
        private DomainValidator _validator;
        private Mock<IDomainDataServices> _domainDataServices;

        [SetUp]
        public void Setup()
        {
            _domainDataServices = new Mock<IDomainDataServices>();
            _validator = new DomainValidator();
            _logMock = new Mock<ILog>();
            _domainServicesImplementation = new DomainServicesImplementation(_logMock.Object, _domainDataServices.Object);
        }

        [Test]
        public void AddDomainValidDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };

            _domainServicesImplementation.AddDomain(domain);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddDomainInvalidDomainTest()
        {
            var domain = new Domain();
            ValidationResult result = _validator.Validate(domain);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _domainServicesImplementation.AddDomain(domain));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteDomainValidDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };

            _domainServicesImplementation.DeleteDomain(domain);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteDomainInexistingDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };
            var expectedExceptionMessage = $"Can't delete domain with id {domain.Id} because it's not in the database.";

            _domainDataServices.Setup(m => m.DeleteDomain(It.IsAny<Domain>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _domainServicesImplementation.DeleteDomain(domain));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateDomainValidDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };

            _domainServicesImplementation.UpdateDomain(domain);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateDomainInexistingDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };
            var expectedExceptionMessage = $"Can't update domain with id {domain.Id} because it's not in the database.";

            _domainDataServices.Setup(m => m.UpdateDomain(It.IsAny<Domain>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _domainServicesImplementation.UpdateDomain(domain));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateDomainInvalidDomainTest()
        {
            var domain = new Domain();
            ValidationResult result = _validator.Validate(domain);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _domainServicesImplementation.UpdateDomain(domain));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllDomainsTest()
        {
            var domain1 = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };
            var domain2 = new Domain()
            {
                Name = "Matematica",
                Parent = domain1,
                ParentId = domain1.Id,
            };

            var domains = new List<Domain>
            {
                domain1,
                domain2
            };
            _domainDataServices.Setup(m => m.GetAllDomains()).Returns(domains);

            var result = _domainServicesImplementation.GetAllDomains();

            Assert.AreEqual(domains.Count, result.Count);
            Assert.That(domains[0], Is.EqualTo(result[0]));
            Assert.That(domains[1], Is.EqualTo(result[1]));
        }

        [Test]
        public void GetDomainByIdExistingDomainTest()
        {
            Domain domain = new Domain();
            _domainDataServices.Setup(m => m.GetDomainById(It.IsAny<int>())).Returns(domain);

            _domainServicesImplementation.GetDomainById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetDomainByIdInexistingDomainTest()
        {
            Domain domain = null;
            _domainDataServices.Setup(m => m.GetDomainById(It.IsAny<int>())).Returns(domain);

            _domainServicesImplementation.GetDomainById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteDomainByIdValidDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };
            _domainDataServices.Setup(m => m.GetDomainById(It.IsAny<int>())).Returns(domain);

            _domainServicesImplementation.DeleteDomainById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteDomainByIdInexistingDomainTest()
        {
            var domain = new Domain
            {
                Name = "Stiinta",
                Parent = null,
            };
            var expectedExceptionMessage = $"Can't delete domain with id {domain.Id} because it's not in the database.";

            _domainDataServices.Setup(m => m.DeleteDomain(It.IsAny<Domain>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _domainServicesImplementation.DeleteDomain(domain));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }
    }
}
