﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class AuthorServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private AuthorServicesImplementation _authorServicesImplementation;
        private AuthorValidator _validator;
        private Mock<IAuthorDataServices> _authorDataServices;

        [SetUp]
        public void Setup()
        {
            _authorDataServices = new Mock<IAuthorDataServices>();
            _validator = new AuthorValidator();
            _logMock = new Mock<ILog>();
            _authorServicesImplementation = new AuthorServicesImplementation(_logMock.Object, _authorDataServices.Object);
        }

        [Test]
        public void AddAuthorValidAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            _authorServicesImplementation.AddAuthor(author);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddAuthorInvalidAuthorTest()
        {
            var author = new Author();
            ValidationResult result = _validator.Validate(author);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.AddAuthor(author));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteAuthorValidAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            _authorServicesImplementation.DeleteAuthor(author);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteAuthorInexistingAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };
            var expectedExceptionMessage = $"Can't delete author with id {author.Id} because it's not in the database.";

            _authorDataServices.Setup(m => m.DeleteAuthor(It.IsAny<Author>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.DeleteAuthor(author));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateAuthorValidAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            _authorServicesImplementation.UpdateAuthor(author);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateAuthorInexistingAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };
            var expectedExceptionMessage = $"Can't update author with id {author.Id} because it's not in the database.";

            _authorDataServices.Setup(m => m.UpdateAuthor(It.IsAny<Author>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.UpdateAuthor(author));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateAuthorInvalidAuthorTest()
        {
            var author = new Author();
            ValidationResult result = _validator.Validate(author);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.UpdateAuthor(author));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllAuthorsTest()
        {
            var authors = new List<Author>
            {
                new Author
                {
                    Firstname = "Stuart",
                    Lastname = "Russell",
                },
            };
            _authorDataServices.Setup(m => m.GetAllAuthors()).Returns(authors);

            var result = _authorServicesImplementation.GetAllAuthors();

            Assert.AreEqual(authors.Count, result.Count);
            Assert.That(authors[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetAuthorByIdExistingAuthorTest()
        {
            Author author = new Author();
            _authorDataServices.Setup(m => m.GetAuthorById(It.IsAny<int>())).Returns(author);

            _authorServicesImplementation.GetAuthorById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetAuthorByIdInexistingAuthorTest()
        {
            Author author = null;
            _authorDataServices.Setup(m => m.GetAuthorById(It.IsAny<int>())).Returns(author);

            _authorServicesImplementation.GetAuthorById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteAuthorByIdValidAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };
            _authorDataServices.Setup(m => m.GetAuthorById(It.IsAny<int>())).Returns(author);

            _authorServicesImplementation.DeleteAuthorById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteAuthorByIdInexistingAuthorTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };
            var expectedExceptionMessage = $"Can't delete author with id {author.Id} because it's not in the database.";

            _authorDataServices.Setup(m => m.DeleteAuthor(It.IsAny<Author>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.DeleteAuthor(author));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }
    }
}
