﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class EditionServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private EditionServicesImplementation _editionServicesImplementation;
        private EditionValidator _validator;
        private Mock<IEditionDataServices> _editionDataServices;

        [SetUp]
        public void Setup()
        {
            _editionDataServices = new Mock<IEditionDataServices>();
            _validator = new EditionValidator();
            _logMock = new Mock<ILog>();
            _editionServicesImplementation = new EditionServicesImplementation(_logMock.Object, _editionDataServices.Object);
        }

        [Test]
        public void AddEditionValidEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
                Book = new Book()
            };

            _editionServicesImplementation.AddEdition(edition);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddEditionInvalidEditionTest()
        {
            var edition = new Edition();
            ValidationResult result = _validator.Validate(edition);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _editionServicesImplementation.AddEdition(edition));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteEditionValidEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
            };

            _editionServicesImplementation.DeleteEdition(edition);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteEditionInexistingEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
            };
            var expectedExceptionMessage = $"Can't delete edition with id {edition.Id} because it's not in the database.";

            _editionDataServices.Setup(m => m.DeleteEdition(It.IsAny<Edition>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _editionServicesImplementation.DeleteEdition(edition));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateEditionValidEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
                Book = new Book()
            };

            _editionServicesImplementation.UpdateEdition(edition);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateEditionInexistingEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
                Book = new Book()
            };
            var expectedExceptionMessage = $"Can't update edition with id {edition.Id} because it's not in the database.";

            _editionDataServices.Setup(m => m.UpdateEdition(It.IsAny<Edition>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _editionServicesImplementation.UpdateEdition(edition));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateEditionInvalidEditionTest()
        {
            var edition = new Edition();
            ValidationResult result = _validator.Validate(edition);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _editionServicesImplementation.UpdateEdition(edition));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllEditionsTest()
        {
            var editions = new List<Edition>
            {
                new Edition
                {
                    Number = 3,
                    Year = 2009,
                    Publisher = "Pearson",
                    NumberOfPages = 1152,
                    Type = "Hardcover",
                }
            };
            _editionDataServices.Setup(m => m.GetAllEditions()).Returns(editions);

            var result = _editionServicesImplementation.GetAllEditions();

            Assert.AreEqual(editions.Count, result.Count);
            Assert.That(editions[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetEditionByIdExistingEditionTest()
        {
            Edition edition = new Edition();
            _editionDataServices.Setup(m => m.GetEditionById(It.IsAny<int>())).Returns(edition);

            _editionServicesImplementation.GetEditionById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetEditionByIdInexistingEditionTest()
        {
            Edition edition = null;
            _editionDataServices.Setup(m => m.GetEditionById(It.IsAny<int>())).Returns(edition);

            _editionServicesImplementation.GetEditionById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteEditionByIdValidEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
            };
            _editionDataServices.Setup(m => m.GetEditionById(It.IsAny<int>())).Returns(edition);

            _editionServicesImplementation.DeleteEditionById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteEditionByIdInexistingEditionTest()
        {
            var edition = new Edition()
            {
                Number = 3,
                Year = 2009,
                Publisher = "Pearson",
                NumberOfPages = 1152,
                Type = "Hardcover",
            };
            var expectedExceptionMessage = $"Can't delete edition with id {edition.Id} because it's not in the database.";

            _editionDataServices.Setup(m => m.DeleteEdition(It.IsAny<Edition>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _editionServicesImplementation.DeleteEdition(edition));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }
    }
}
