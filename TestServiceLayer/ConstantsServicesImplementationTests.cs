﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class ConstantsServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private ConstantsServicesImplementation _constantsServicesImplementation;
        private ConstantsValidator _validator;
        private Mock<IConstantsDataServices> _constantsDataServices;

        [SetUp]
        public void Setup()
        {
            _constantsDataServices = new Mock<IConstantsDataServices>();
            _validator = new ConstantsValidator();
            _logMock = new Mock<ILog>();
            _constantsServicesImplementation = new ConstantsServicesImplementation(_logMock.Object, _constantsDataServices.Object);
        }

        [Test]
        public void AddConstantsValidConstantsEmptyDatabaseTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };

            _constantsDataServices.Setup(m => m.GetAllConstants()).Returns(new List<Constants>());

            _constantsServicesImplementation.AddConstants(constants);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
            _constantsDataServices.Verify(x => x.GetLatestConstants(), Times.Never());
        }

        [Test]
        public void AddConstantsValidConstantsNotEmptyDatabaseTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };

            _constantsDataServices.Setup(m => m.GetAllConstants()).Returns(new List<Constants>
            {
                new Constants()
            });

            _constantsDataServices.Setup(m => m.GetLatestConstants()).Returns(new Constants());

            _constantsServicesImplementation.AddConstants(constants);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
            _constantsDataServices.Verify(x => x.GetLatestConstants(), Times.Once());
        }

        [Test]
        public void AddConstantsInvalidConstantsTest()
        {
            var constants = new Constants();
            ValidationResult result = _validator.Validate(constants);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _constantsServicesImplementation.AddConstants(constants));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteConstantsValidConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };

            _constantsServicesImplementation.DeleteConstants(constants);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteConstantsInexistingConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            var expectedExceptionMessage = $"Can't delete constants with id {constants.Id} because it's not in the database.";

            _constantsDataServices.Setup(m => m.DeleteConstants(It.IsAny<Constants>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _constantsServicesImplementation.DeleteConstants(constants));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateConstantsValidConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };

            _constantsServicesImplementation.UpdateConstants(constants);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateConstantsInexistingConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            var expectedExceptionMessage = $"Can't update constants with id {constants.Id} because it's not in the database.";

            _constantsDataServices.Setup(m => m.UpdateConstants(It.IsAny<Constants>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _constantsServicesImplementation.UpdateConstants(constants));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateConstantsInvalidConstantsTest()
        {
            var constants = new Constants();
            ValidationResult result = _validator.Validate(constants);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _constantsServicesImplementation.UpdateConstants(constants));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllConstantssTest()
        {
            var constantss = new List<Constants>
            {
                new Constants
                {
                    DOM = 3,
                    NMC = 10,
                    PER = 1,
                    C = 2,
                    D = 2,
                    L = 1,
                    LIM = 3,
                    DELTA = 1,
                    NCZ = 5,
                },
            };
            _constantsDataServices.Setup(m => m.GetAllConstants()).Returns(constantss);

            var result = _constantsServicesImplementation.GetAllConstants();

            Assert.AreEqual(constantss.Count, result.Count);
            Assert.That(constantss[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetConstantsByIdExistingConstantsTest()
        {
            Constants constants = new Constants();
            _constantsDataServices.Setup(m => m.GetConstantsById(It.IsAny<int>())).Returns(constants);

            _constantsServicesImplementation.GetConstantsById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetConstantsByIdInexistingConstantsTest()
        {
            Constants constants = null;
            _constantsDataServices.Setup(m => m.GetConstantsById(It.IsAny<int>())).Returns(constants);

            _constantsServicesImplementation.GetConstantsById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteConstantsByIdValidConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            _constantsDataServices.Setup(m => m.GetConstantsById(It.IsAny<int>())).Returns(constants);

            _constantsServicesImplementation.DeleteConstantsById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteConstantsByIdInexistingConstantsTest()
        {
            var constants = new Constants
            {
                DOM = 3,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            var expectedExceptionMessage = $"Can't delete constants with id {constants.Id} because it's not in the database.";

            _constantsDataServices.Setup(m => m.DeleteConstants(It.IsAny<Constants>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _constantsServicesImplementation.DeleteConstants(constants));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetLatestConstantsExistingConstantsTest()
        {
            Constants constants = new Constants();
            _constantsDataServices.Setup(m => m.GetLatestConstants()).Returns(constants);

            _constantsServicesImplementation.GetLatestConstants();

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetLatestConstantsInexistingConstantsTest()
        {
            Constants constants = null;
            _constantsDataServices.Setup(m => m.GetLatestConstants()).Returns(constants);

            _constantsServicesImplementation.GetLatestConstants();

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }
    }
}
