﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class LibraryServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private LibraryServicesImplementation _libraryServicesImplementation;
        private LibraryValidator _validator;
        private Mock<ILibraryDataServices> _libraryDataServices;

        [SetUp]
        public void Setup()
        {
            _libraryDataServices = new Mock<ILibraryDataServices>();
            _validator = new LibraryValidator();
            _logMock = new Mock<ILog>();
            _libraryServicesImplementation = new LibraryServicesImplementation(_logMock.Object, _libraryDataServices.Object);
        }

        [Test]
        public void AddLibraryValidLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                Edition = new Edition(),
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };

            _libraryServicesImplementation.AddLibrary(library);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddLibraryInvalidLibraryTest()
        {
            var library = new Library();
            ValidationResult result = _validator.Validate(library);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _libraryServicesImplementation.AddLibrary(library));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteLibraryValidLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };

            _libraryServicesImplementation.DeleteLibrary(library);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteLibraryInexistingLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };
            var expectedExceptionMessage = $"Can't delete library with id {library.EditionId} because it's not in the database.";

            _libraryDataServices.Setup(m => m.DeleteLibrary(It.IsAny<Library>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _libraryServicesImplementation.DeleteLibrary(library));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateLibraryValidLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                Edition = new Edition(),
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };

            _libraryServicesImplementation.UpdateLibrary(library);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateLibraryInexistingLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                Edition = new Edition(),
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };
            var expectedExceptionMessage = $"Can't update library with id {library.EditionId} because it's not in the database.";

            _libraryDataServices.Setup(m => m.UpdateLibrary(It.IsAny<Library>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _libraryServicesImplementation.UpdateLibrary(library));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateLibraryInvalidLibraryTest()
        {
            var library = new Library();
            ValidationResult result = _validator.Validate(library);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _libraryServicesImplementation.UpdateLibrary(library));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllLibrarysTest()
        {
            var librarys = new List<Library>
            {
                new Library
                {
                    EditionId = 1,
                    TotalNumberOfCopies = 50,
                    NumberOfCopiesReadingRoom = 20,
                },
            };
            _libraryDataServices.Setup(m => m.GetAllLibrarys()).Returns(librarys);

            var result = _libraryServicesImplementation.GetAllLibrarys();

            Assert.AreEqual(librarys.Count, result.Count);
            Assert.That(librarys[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetLibraryByIdExistingLibraryTest()
        {
            Library library = new Library();
            _libraryDataServices.Setup(m => m.GetLibraryById(It.IsAny<int>())).Returns(library);

            _libraryServicesImplementation.GetLibraryById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetLibraryByIdInexistingLibraryTest()
        {
            Library library = null;
            _libraryDataServices.Setup(m => m.GetLibraryById(It.IsAny<int>())).Returns(library);

            _libraryServicesImplementation.GetLibraryById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteLibraryByIdValidLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };
            _libraryDataServices.Setup(m => m.GetLibraryById(It.IsAny<int>())).Returns(library);

            _libraryServicesImplementation.DeleteLibraryById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteLibraryByIdInexistingLibraryTest()
        {
            var library = new Library
            {
                EditionId = 1,
                TotalNumberOfCopies = 50,
                NumberOfCopiesReadingRoom = 20,
            };
            var expectedExceptionMessage = $"Can't delete library with id {library.EditionId} because it's not in the database.";

            _libraryDataServices.Setup(m => m.DeleteLibrary(It.IsAny<Library>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _libraryServicesImplementation.DeleteLibrary(library));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }
    }
}
