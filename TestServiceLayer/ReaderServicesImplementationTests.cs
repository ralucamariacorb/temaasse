﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class ReaderServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private ReaderServicesImplementation _readerServicesImplementation;
        private ReaderValidator _validator;
        private Mock<IReaderDataServices> _readerDataServices;

        [SetUp]
        public void Setup()
        {
            _readerDataServices = new Mock<IReaderDataServices>();
            _validator = new ReaderValidator();
            _logMock = new Mock<ILog>();
            _readerServicesImplementation = new ReaderServicesImplementation(_logMock.Object, _readerDataServices.Object);
        }

        [Test]
        public void AddReaderValidReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
                JoinDate = DateTime.Now.AddDays(-1)
            };

            _readerServicesImplementation.AddReader(reader);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddReaderInvalidReaderTest()
        {
            var reader = new Reader
            {
                JoinDate = DateTime.Now.AddDays(-1)
            };

            ValidationResult result = _validator.Validate(reader);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _readerServicesImplementation.AddReader(reader));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteReaderValidReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
            };

            _readerServicesImplementation.DeleteReader(reader);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteReaderInexistingReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
                JoinDate = DateTime.Now.AddDays(-1)
            };
            var expectedExceptionMessage = $"Can't delete reader with id {reader.Id} because it's not in the database.";

            _readerDataServices.Setup(m => m.DeleteReader(It.IsAny<Reader>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _readerServicesImplementation.DeleteReader(reader));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateReaderValidReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
                JoinDate = DateTime.Now.AddDays(-1)
            };

            _readerServicesImplementation.UpdateReader(reader);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateReaderInexistingReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
                JoinDate = DateTime.Now.AddDays(-1)
            };
            var expectedExceptionMessage = $"Can't update reader with id {reader.Id} because it's not in the database.";

            _readerDataServices.Setup(m => m.UpdateReader(It.IsAny<Reader>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _readerServicesImplementation.UpdateReader(reader));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateReaderInvalidReaderTest()
        {
            var reader = new Reader
            {
                JoinDate = DateTime.Now.AddDays(-1)
            };
            ValidationResult result = _validator.Validate(reader);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _readerServicesImplementation.UpdateReader(reader));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllReadersTest()
        {
            var readers = new List<Reader>
            {
                new Reader
                {
                    Lastname = "ReaderLastname",
                    Firstname = "ReaderFirstname",
                    Address = "ReaderAddress",
                    Email = null,
                    PhoneNumber = "1234567890",
                },
            };
            _readerDataServices.Setup(m => m.GetAllReaders()).Returns(readers);

            var result = _readerServicesImplementation.GetAllReaders();

            Assert.AreEqual(readers.Count, result.Count);
            Assert.That(readers[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetReaderByIdExistingReaderTest()
        {
            Reader reader = new Reader();
            _readerDataServices.Setup(m => m.GetReaderById(It.IsAny<int>())).Returns(reader);

            _readerServicesImplementation.GetReaderById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetReaderByIdInexistingReaderTest()
        {
            Reader reader = null;
            _readerDataServices.Setup(m => m.GetReaderById(It.IsAny<int>())).Returns(reader);

            _readerServicesImplementation.GetReaderById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteReaderByIdValidReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
            };
            _readerDataServices.Setup(m => m.GetReaderById(It.IsAny<int>())).Returns(reader);

            _readerServicesImplementation.DeleteReaderById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteReaderByIdInexistingReaderTest()
        {
            var reader = new Reader
            {
                Lastname = "ReaderLastname",
                Firstname = "ReaderFirstname",
                Address = "ReaderAddress",
                Email = null,
                PhoneNumber = "1234567890",
            };
            var expectedExceptionMessage = $"Can't delete reader with id {reader.Id} because it's not in the database.";

            _readerDataServices.Setup(m => m.DeleteReader(It.IsAny<Reader>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _readerServicesImplementation.DeleteReader(reader));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }
    }
}
