﻿using System;
using System.Collections.Generic;
using DataMapper;
using DomainModel;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer;
using ServiceLayer.ServiceImplementation;
using Program = ServiceLayer.Program;

namespace TestServiceLayer
{
    class ProgramTests
    {
        private Program _program;
        private Mock<IAuthorServices> authorServices;
        private Mock<IDomainServices> domainServices;
        private Mock<IEditionServices> editionServices;
        private Mock<IBookServices> bookServices;
        private Mock<IConstantsServices> constantsServices;
        private Mock<ILibraryServices> libraryServices;
        private Mock<IReaderServices> readerServices;
        private Mock<IEditionReaderServices> editionReaderServices;
        private Constants _constants;
        private List<Domain> _domains;


        [SetUp]
        public void Setup()
        {
            var domain1 = new Domain()
            {
                Id = 0,
                Name = "Stiinta",
                Parent = null,
            };
            var domain2 = new Domain()
            {
                Id = 1,
                Name = "Matematica",
                Parent = domain1,
                ParentId = domain1.Id,
            };
            var domain3 = new Domain()
            {
                Id = 2,
                Name = "Informatica",
                Parent = domain1,
                ParentId = domain1.Id,
            };
            var domain4 = new Domain()
            {
                Id = 3,
                Name = "Algoritmi",
                Parent = domain3,
                ParentId = domain3.Id,
            };
            var domain5 = new Domain()
            {
                Id = 4,
                Name = "Retele",
                Parent = domain3,
                ParentId = domain3.Id,
            };
            var domain6 = new Domain()
            {
                Id = 5,
                Name = "Algoritmica grafurilor",
                Parent = domain4,
                ParentId = domain4.Id,
            };
            var domain7 = new Domain()
            {
                Id = 6,
                Name = "Algoritmi cuantici",
                Parent = domain4,
                ParentId = domain4.Id,
            };
            var domain8 = new Domain()
            {
                Id = 7,
                Name = "Arta",
                Parent = null,
            };

            _domains = new List<Domain>
            {
                domain1,
                domain2,
                domain3,
                domain4,
                domain5,
                domain6,
                domain7,
                domain8
            };

            domainServices = new Mock<IDomainServices>();
            domainServices.Setup(m => m.GetAllDomains()).Returns(_domains);
            domainServices.Setup(m => m.GetDomainById(0)).Returns(_domains[0]);
            domainServices.Setup(m => m.GetDomainById(1)).Returns(_domains[1]);
            domainServices.Setup(m => m.GetDomainById(2)).Returns(_domains[2]);
            domainServices.Setup(m => m.GetDomainById(3)).Returns(_domains[3]);
            domainServices.Setup(m => m.GetDomainById(4)).Returns(_domains[4]);
            domainServices.Setup(m => m.GetDomainById(5)).Returns(_domains[5]);
            domainServices.Setup(m => m.GetDomainById(6)).Returns(_domains[6]);
            domainServices.Setup(m => m.GetDomainById(7)).Returns(_domains[7]);


            _constants = new Constants
            {
                DOM = 3,
                NMC = 4,
                PER = 7,
                C = 5,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 2,
                NCZ = 5,
            };

            constantsServices = new Mock<IConstantsServices>();
            constantsServices.Setup(m => m.GetLatestConstants()).Returns(_constants);

            authorServices = new Mock<IAuthorServices>();
            bookServices = new Mock<IBookServices>();
            editionServices = new Mock<IEditionServices>();
            libraryServices = new Mock<ILibraryServices>();
            readerServices = new Mock<IReaderServices>();
            editionReaderServices = new Mock<IEditionReaderServices>();

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
                );
        }

        [Test]
        public void CanAddDomainFailToAddParentDomainTest()
        {
            var domain = new Domain()
            {
                Id = 2,
                Name = "Informatica",
                ParentId = 1
            };

            var book = new Book
            {
                Id = 1,
            };

            book.Domains.Add(new BookDomain
            {
                Domain = domain,
                DomainId = domain.Id,
                Book = book,
                BookId = book.Id
            });

            var domain1 = new Domain()
            {
                Id = 0,
                Name = "Stiinta",
                Parent = null,
            };

            var result = Program.CanAddDomain(book, domain1);
            Assert.IsFalse(result);
        }

        [Test]
        public void CanAddDomainFailToAddChildDomainTest()
        {
            var domain = new Domain()
            {
                Id = 2,
                Name = "Informatica",
                ParentId = 1
            };

            var book = new Book
            {
                Id = 1,
            };

            book.Domains.Add(new BookDomain
            {
                Domain = domain,
                DomainId = domain.Id,
                Book = book,
                BookId = book.Id
            });

            var domain1 = new Domain()
            {
                Id = 6,
                Name = "Algoritmi cuantici",
                ParentId = 4
            };

            var result = Program.CanAddDomain(book, domain1);
            Assert.IsFalse(result);
        }

        [Test]
        public void CanAddDomainUnrelatedToCurrentDomainTest()
        {
            var domain = new Domain()
            {
                Id = 2,
                Name = "Informatica",
                ParentId = 1
            };

            var book = new Book
            {
                Id = 1,
            };

            book.Domains.Add(new BookDomain
            {
                Domain = domain,
                DomainId = domain.Id,
                Book = book,
                BookId = book.Id
            });

            var domain1 = new Domain()
            {
                Id = 1,
                Name = "Matematica",
                ParentId = 1
            };

            var result = Program.CanAddDomain(book, domain1);
            Assert.IsTrue(result);
        }

        [Test]
        public void CheckNMCandPERConditionSuccessTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckNMCandPERCondition(reader, editions));
        }

        [Test]
        public void CheckNMCandPERConditionFailTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );
            string exceptionMessage =
                $"You can't borrow more than {_constants.NMC} editions in {_constants.PER} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckNMCandPERCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckNMCandPERConditionStaffSuccessTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckNMCandPERCondition(reader, editions));
        }

        [Test]
        public void CheckNMCandPERConditionStaffFailTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );
            string exceptionMessage =
                $"You can't borrow more than {_constants.NMC * 2} editions in {_constants.PER / 2} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckNMCandPERCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// Borrow 1 edition.
        /// </summary>
        [Test]
        public void CheckCConditionSuccessTest()
        {
            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition()
            };

            Assert.DoesNotThrow(() => _program.CheckCCondition(reader, editions));
        }

        /// <summary>
        /// Borrow [2 Informatica, 1 Arta].
        /// </summary>
        [Test]
        public void CheckCConditionSuccess1Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var artBook = new Book
            {
                Id = 2,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[7].Id
                    }
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = artBook
                }
            };

            bookServices.Setup(m => m.GetBookById(1)).Returns(infoBook);
            bookServices.Setup(m => m.GetBookById(2)).Returns(artBook);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckCCondition(reader, editions));
        }

        [Test]
        public void CheckCConditionFailCTest()
        {
            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition()
            };

            string exceptionMessage =
                $"You can't borrow {editions.Count} editions. You can borrow maximum {_constants.C}.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckCCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckCConditionFailDomainsTest()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                }
            };

            bookServices.Setup(m => m.GetBookById(1)).Returns(infoBook);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            string exceptionMessage = "You need to have editions from at least 2 domains.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckCCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// Borrow 1 edition.
        /// </summary>
        [Test]
        public void CheckCConditionSuccessStaffTest()
        {
            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                new Edition()
            };

            Assert.DoesNotThrow(() => _program.CheckCCondition(reader, editions));
        }

        /// <summary>
        /// Borrow [2 Informatica, 1 Arta].
        /// </summary>
        [Test]
        public void CheckCConditionSuccessStaff1Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var artBook = new Book
            {
                Id = 2,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[7].Id
                    }
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = artBook
                }
            };

            bookServices.Setup(m => m.GetBookById(1)).Returns(infoBook);
            bookServices.Setup(m => m.GetBookById(2)).Returns(artBook);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckCCondition(reader, editions));
        }

        [Test]
        public void CheckCConditionFailCStaffTest()
        {
            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition()
            };

            string exceptionMessage =
                $"You can't borrow {editions.Count} editions. You can borrow maximum {_constants.C}.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckCCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckCConditionFailDomainsStaffTest()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                },
                new Edition
                {
                    Book = infoBook
                }
            };

            bookServices.Setup(m => m.GetBookById(1)).Returns(infoBook);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            string exceptionMessage = "You need to have editions from at least 2 domains.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckCCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica, 1 Arta] book and wants to borrow [1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionSuccessTest()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var artBook = new Book
            {
                Id = 2,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[7].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = artBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                },
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition2.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition3
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            editionServices.Setup(m => m.GetEditionById(2)).Returns(edition2);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckDandLCondition(reader, editions));
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica] book and wants to borrow [1 Arta, 1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionSuccess1Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var artBook = new Book
            {
                Id = 2,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[7].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = artBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition2,
                edition3
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckDandLCondition(reader, editions));
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica, 1 Informatica] book and wants to borrow [1 Arta] 
        /// </summary>
        [Test]
        public void CheckDandLConditionSuccess2Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var artBook = new Book
            {
                Id = 2,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[7].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = artBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                },
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition3.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition2
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            editionServices.Setup(m => m.GetEditionById(3)).Returns(edition3);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckDandLCondition(reader, editions));
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica] book and wants to borrow [1 Informatica, 1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionFailTest()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = infoBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition2,
                edition3
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            var exceptionMessage = $"You can't borrow more than {_constants.D} editions from the same domain in the last {_constants.L} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDandLCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica, 1 Informatica] book and wants to borrow [1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionFail1Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = infoBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                },
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition2.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition3
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            editionServices.Setup(m => m.GetEditionById(2)).Returns(edition2);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            var exceptionMessage = $"You can't borrow more than {_constants.D} editions from the same domain in the last {_constants.L} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDandLCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica, 1 Informatica, 1 Informatica] book and wants to borrow [1 Informatica, 1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionFailStaffTest()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = infoBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };
            var edition4 = new Edition
            {
                Id = 4,
                Book = infoBook
            };
            var edition5 = new Edition
            {
                Id = 5,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                },
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition2.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
                ,new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition3.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }

            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                edition4,
                edition5
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            editionServices.Setup(m => m.GetEditionById(2)).Returns(edition2);
            editionServices.Setup(m => m.GetEditionById(3)).Returns(edition3);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            var exceptionMessage = $"You can't borrow more than {_constants.D * 2} editions from the same domain in the last {_constants.L} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDandLCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        /// <summary>
        /// D = 2 Reader already borrowed [1 Informatica, 1 Informatica] book and wants to borrow [1 Informatica, 1 Informatica, 1 Informatica] 
        /// </summary>
        [Test]
        public void CheckDandLConditionFailStaff1Test()
        {
            var infoBook = new Book
            {
                Id = 1,
                Domains = new List<BookDomain>
                {
                    new BookDomain
                    {
                        DomainId = _domains[2].Id
                    }
                }
            };

            var edition1 = new Edition
            {
                Id = 1,
                Book = infoBook
            };
            var edition2 = new Edition
            {
                Id = 2,
                Book = infoBook
            };
            var edition3 = new Edition
            {
                Id = 3,
                Book = infoBook
            };
            var edition4 = new Edition
            {
                Id = 4,
                Book = infoBook
            };
            var edition5 = new Edition
            {
                Id = 5,
                Book = infoBook
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                },
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition2.Id,
                    BorrowTime = DateTime.Now.AddDays(-1)
                }
            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                edition3,
                edition4,
                edition5
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);
            editionServices.Setup(m => m.GetEditionById(1)).Returns(edition1);
            editionServices.Setup(m => m.GetEditionById(2)).Returns(edition2);
            bookServices.Setup(m => m.GetBookById(1)).Returns(edition1.Book);
            bookServices.Setup(m => m.GetBookById(2)).Returns(edition2.Book);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            var exceptionMessage = $"You can't borrow more than {_constants.D * 2} editions from the same domain in the last {_constants.L} months.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDandLCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckDELTAConditionSuccessTest()
        {
            var edition1 = new Edition
            {
                Id = 1
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-5)
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition1
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckDELTACondition(reader, editions));
        }

        [Test]
        public void CheckDELTAConditionFailTest()
        {
            var edition1 = new Edition
            {
                Id = 1
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                edition1
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );
            string exceptionMessage = $"You can't borrow edition with id {edition1.Id} now. {_constants.DELTA} days didn't passed since last time you borrowed this edition.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDELTACondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckDELTAConditionSuccessStaffTest()
        {
            var edition1 = new Edition
            {
                Id = 1
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now.AddDays(-5)
                }
            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                edition1
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckDELTACondition(reader, editions));
        }

        [Test]
        public void CheckDELTAConditionFailStaffTest()
        {
            var edition1 = new Edition
            {
                Id = 1
            };

            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    EditionId = edition1.Id,
                    BorrowTime = DateTime.Now
                }
            };

            var reader = new Reader
            {
                Id = 1,
                IsLibraryStaff = true
            };

            var editions = new List<Edition>
            {
                edition1
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );
            string exceptionMessage = $"You can't borrow edition with id {edition1.Id} now. {_constants.DELTA / 2} days didn't passed since last time you borrowed this edition.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckDELTACondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

        [Test]
        public void CheckNCZConditionSuccessTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );

            Assert.DoesNotThrow(() => _program.CheckNCZCondition(reader, editions));
        }

        [Test]
        public void CheckNCZConditionFailTest()
        {
            var editionReaders = new List<EditionReader>
            {
                new EditionReader
                {
                    ReaderId = 1,
                    BorrowTime = DateTime.Now
                }
            };

            var reader = new Reader
            {
                Id = 1
            };

            var editions = new List<Edition>
            {
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition(),
                new Edition()
            };

            editionReaderServices.Setup(m => m.GetAllEditionReaders()).Returns(editionReaders);

            _program = new Program(
                authorServices.Object,
                domainServices.Object,
                editionServices.Object,
                bookServices.Object,
                constantsServices.Object,
                libraryServices.Object,
                readerServices.Object,
                editionReaderServices.Object
            );
            string exceptionMessage = $"You can't borrow more than {_constants.NCZ} editions in one day.";

            Exception result = Assert.Throws<Exception>(() => _program.CheckNCZCondition(reader, editions));
            Assert.AreEqual(exceptionMessage, result.Message);
        }

    }
}
