﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMapper;
using DomainModel;
using DomainModel.Validators;
using FluentValidation.Results;
using log4net;
using Moq;
using NUnit.Framework;
using ServiceLayer.ServiceImplementation;

namespace TestServiceLayer
{
    public class BookServicesImplementationTests
    {
        private Mock<ILog> _logMock;
        private BookServicesImplementation _authorServicesImplementation;
        private BookValidator _validator;
        private Mock<IBookDataServices> _bookDataServices;
        private Mock<IConstantsDataServices> _constantsDataServices;

        [SetUp]
        public void Setup()
        {
            _bookDataServices = new Mock<IBookDataServices>();
            _constantsDataServices = new Mock<IConstantsDataServices>();
            _validator = new BookValidator();
            _logMock = new Mock<ILog>();
            _authorServicesImplementation = new BookServicesImplementation(_logMock.Object, _bookDataServices.Object, _constantsDataServices.Object);

            Constants constants = new Constants
            {
                DOM = 1,
                NMC = 10,
                PER = 1,
                C = 2,
                D = 2,
                L = 1,
                LIM = 3,
                DELTA = 1,
                NCZ = 5,
            };
            _constantsDataServices.Setup(m => m.GetLatestConstants()).Returns(constants);
        }

        [Test]
        public void AddBookValidBookTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            var domain = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };

            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 3,
                        Year = 2009,
                        Publisher = "Pearson",
                        NumberOfPages = 1152,
                        Type = "Hardcover",
                    },
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain.Id,
                BookId = book.Id,
            });

            _authorServicesImplementation.AddBook(book);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void AddBookTooManyDomainsTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            var domain1 = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };
            var domain2 = new Domain()
            {
                Name = "Matematica",
                Parent = domain1,
                ParentId = domain1.Id,
            };

            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 3,
                        Year = 2009,
                        Publisher = "Pearson",
                        NumberOfPages = 1152,
                        Type = "Hardcover",
                    },
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain1.Id,
                BookId = book.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain2.Id,
                BookId = book.Id,
            });

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.AddBook(book));
            Assert.That(exception.Message, Is.EqualTo($"Book with title {book.Title} can't be added because it has too many domains."));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void AddBookInvalidBookTest()
        {
            var book = new Book();
            ValidationResult result = _validator.Validate(book);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.AddBook(book));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void DeleteBookValidBookTest()
        {
            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
            };

            _authorServicesImplementation.DeleteBook(book);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteBookInexistingBookTest()
        {
            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
            };
            var expectedExceptionMessage = $"Can't delete author with id {book.Id} because it's not in the database.";

            _bookDataServices.Setup(m => m.DeleteBook(It.IsAny<Book>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.DeleteBook(book));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateBookValidBookTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            var domain = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };

            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 3,
                        Year = 2009,
                        Publisher = "Pearson",
                        NumberOfPages = 1152,
                        Type = "Hardcover",
                    },
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain.Id,
                BookId = book.Id,
            });

            _authorServicesImplementation.UpdateBook(book);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void UpdateBookTooManyDomainsTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            var domain1 = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };
            var domain2 = new Domain()
            {
                Name = "Matematica",
                Parent = domain1,
                ParentId = domain1.Id,
            };

            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 3,
                        Year = 2009,
                        Publisher = "Pearson",
                        NumberOfPages = 1152,
                        Type = "Hardcover",
                    },
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain1.Id,
                BookId = book.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain2.Id,
                BookId = book.Id,
            });

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.UpdateBook(book));
            Assert.That(exception.Message, Is.EqualTo($"Book with title {book.Title} can't be updated because it has too many domains."));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateBookInexistingBookTest()
        {
            var author = new Author
            {
                Firstname = "Stuart",
                Lastname = "Russell",
            };

            var domain = new Domain()
            {
                Name = "Stiinta",
                Parent = null,
            };

            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
                Editions = new List<Edition>
                {
                    new Edition()
                    {
                        Number = 3,
                        Year = 2009,
                        Publisher = "Pearson",
                        NumberOfPages = 1152,
                        Type = "Hardcover",
                    },
                },
            };

            book.Authors.Add(new BookAuthor
            {
                BookId = book.Id,
                AuthorId = author.Id,
            });

            book.Domains.Add(new BookDomain
            {
                DomainId = domain.Id,
                BookId = book.Id,
            });
            var expectedExceptionMessage = $"Can't update author with id {book.Id} because it's not in the database.";

            _bookDataServices.Setup(m => m.UpdateBook(It.IsAny<Book>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.UpdateBook(book));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void UpdateBookInvalidBookTest()
        {
            var author = new Book();
            ValidationResult result = _validator.Validate(author);
            StringBuilder errors = new StringBuilder();
            foreach (var error in result.Errors)
            {
                errors.Append(error);
            }

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.UpdateBook(author));
            Assert.That(exception.Message, Is.EqualTo(errors.ToString()));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

        [Test]
        public void GetAllBooksTest()
        {
            var books = new List<Book>
            {
                new Book
                {
                    Title = "Artificial Intelligence: A Modern Approach",
                    Editions = new List<Edition>
                    {
                        new Edition
                        {
                            Number = 3,
                            Year = 2009,
                            Publisher = "Pearson",
                            NumberOfPages = 1152,
                            Type = "Hardcover",

                        },
                    },
                },
            };

            _bookDataServices.Setup(m => m.GetAllBooks()).Returns(books);

            var result = _authorServicesImplementation.GetAllBooks();

            Assert.AreEqual(books.Count, result.Count);
            Assert.That(books[0], Is.EqualTo(result[0]));
        }

        [Test]
        public void GetBookByIdExistingBookTest()
        {
            Book author = new Book();
            _bookDataServices.Setup(m => m.GetBookById(It.IsAny<int>())).Returns(author);

            _authorServicesImplementation.GetBookById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetBookByIdInexistingBookTest()
        {
            Book author = null;
            _bookDataServices.Setup(m => m.GetBookById(It.IsAny<int>())).Returns(author);

            _authorServicesImplementation.GetBookById(1);

            _logMock.Verify(x => x.Warn(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void DeleteBookByIdValidBookTest()
        {
            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
            };

            _bookDataServices.Setup(m => m.GetBookById(It.IsAny<int>())).Returns(book);

            _authorServicesImplementation.DeleteBookById(1);

            _logMock.Verify(x => x.Info(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void DeleteBookByIdInexistingBookTest()
        {
            var book = new Book
            {
                Title = "Artificial Intelligence: A Modern Approach",
            };
            var expectedExceptionMessage = $"Can't delete author with id {book.Id} because it's not in the database.";

            _bookDataServices.Setup(m => m.DeleteBook(It.IsAny<Book>())).Throws(new Exception(expectedExceptionMessage));

            var exception = Assert.Throws<Exception>(() => _authorServicesImplementation.DeleteBook(book));
            Assert.That(exception.Message, Is.EqualTo(expectedExceptionMessage));

            _logMock.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once());
        }

    }
}
