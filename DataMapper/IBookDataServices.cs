﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBookDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IBookDataServices interface.
    /// </summary>
    public interface IBookDataServices
    {
        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns>All books.</returns>
        IList<Book> GetAllBooks();

        /// <summary>
        /// Adds the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void AddBook(Book book);

        /// <summary>
        /// Deletes the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void DeleteBook(Book book);

        /// <summary>
        /// Updates the book.
        /// </summary>
        /// <param name="book">The book.</param>
        void UpdateBook(Book book);

        /// <summary>
        /// Gets the book by given id.
        /// </summary>
        /// <param name="id">The book id.</param>
        /// <returns>The book or null if there is no book with given id.</returns>
        Book GetBookById(int id);
    }
}
