﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReaderDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IReaderDataServices interface.
    /// </summary>
    public interface IReaderDataServices
    {
        /// <summary>
        /// Gets all readers.
        /// </summary>
        /// <returns>All readers.</returns>
        IList<Reader> GetAllReaders();

        /// <summary>
        /// Adds the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        void AddReader(Reader reader);

        /// <summary>
        /// Deletes the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        void DeleteReader(Reader reader);

        /// <summary>
        /// Updates the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        void UpdateReader(Reader reader);

        /// <summary>
        /// Gets the reader by given id.
        /// </summary>
        /// <param name="id">The reader id.</param>
        /// <returns>The reader or null if there is no reader with given id.</returns>
        Reader GetReaderById(int id);
    }
}