﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBookDomainDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IBookDomainDomainDataServices interface.
    /// </summary>
    public interface IBookDomainDataServices
    {
        /// <summary>
        /// Gets all bookDomains.
        /// </summary>
        /// <returns>All bookDomains.</returns>
        IList<BookDomain> GetAllBookDomains();

        /// <summary>
        /// Adds the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void AddBookDomain(BookDomain bookDomain);

        /// <summary>
        /// Deletes the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void DeleteBookDomain(BookDomain bookDomain);

        /// <summary>
        /// Updates the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        void UpdateBookDomain(BookDomain bookDomain);
    }
}