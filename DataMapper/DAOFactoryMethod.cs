﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DAOFactoryMethod.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Configuration;
    using DataMapper.MySqlDAO;

    /// <summary>
    /// DAOFactoryMethod static class.
    /// </summary>
    public static class DAOFactoryMethod
    {
        /// <summary>
        /// Current IDAOFactory.
        /// </summary>
        private static IDAOFactory currentDAOFactory;

        static DAOFactoryMethod()
        {
            string currentDataProvider = ConfigurationManager.AppSettings["dataProvider"];
            if (string.IsNullOrWhiteSpace(currentDataProvider))
            {
                currentDAOFactory = null;
            }
            else
            {
                currentDAOFactory = currentDataProvider.ToLower().Trim() switch
                {
                    "sql" => new SqlDAOFactory(),
                    _ => new SqlDAOFactory(),
                };
            }
        }

        /// <summary>Gets the current DAO factory.</summary>
        /// <value>The current DAO factory.</value>
        public static IDAOFactory CurrentDAOFactory
        {
            get
            {
                currentDAOFactory ??= new SqlDAOFactory();
                return currentDAOFactory;
            }
        }
    }
}
