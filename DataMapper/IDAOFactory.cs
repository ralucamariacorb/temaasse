﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDAOFactory.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    /// <summary>
    /// IDAOFactory interface.
    /// </summary>
    public interface IDAOFactory
    {
        /// <summary>Gets the author data services.</summary>
        /// <value>The author data services.</value>
        IAuthorDataServices AuthorDataServices
        {
            get;
        }

        /// <summary>Gets the bookAuthor data services.</summary>
        /// <value>The bookAuthor data services.</value>
        IBookAuthorDataServices BookAuthorDataServices
        {
            get;
        }

        /// <summary>Gets the book data services.</summary>
        /// <value>The book data services.</value>
        IBookDataServices BookDataServices
        {
            get;
        }

        /// <summary>Gets the bookDomain data services.</summary>
        /// <value>The bookDomain data services.</value>
        IBookDomainDataServices BookDomainDataServices
        {
            get;
        }

        /// <summary>Gets the editionReader data services.</summary>
        /// <value>The editionReader data services.</value>
        IEditionReaderDataServices EditionReaderDataServices
        {
            get;
        }

        /// <summary>Gets the constants data services.</summary>
        /// <value>The constants data services.</value>
        IConstantsDataServices ConstantsDataServices
        {
            get;
        }

        /// <summary>Gets the domain data services.</summary>
        /// <value>The domain data services.</value>
        IDomainDataServices DomainDataServices
        {
            get;
        }

        /// <summary>Gets the edition data services.</summary>
        /// <value>The edition data services.</value>
        IEditionDataServices EditionDataServices
        {
            get;
        }

        /// <summary>Gets the library data services.</summary>
        /// <value>The library data services.</value>
        ILibraryDataServices LibraryDataServices
        {
            get;
        }

        /// <summary>Gets the reader data services.</summary>
        /// <value>The reader data services.</value>
        IReaderDataServices ReaderDataServices
        {
            get;
        }
    }
}
