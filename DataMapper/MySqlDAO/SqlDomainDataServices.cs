﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlDomainDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// SqlDomainDataServices class.
    /// </summary>
    public class SqlDomainDataServices : IDomainDataServices
    {
        /// <summary>
        /// Adds the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void AddDomain(Domain domain)
        {
            using (var context = new LibraryContext())
            {
                if (domain.ParentId == null)
                {
                    context.Domain.Add(domain);
                    context.SaveChanges();
                }
                else
                {
                    Domain parentDomain = context.Domain.Include(x => x.SubDomains).First(x => x.Id == domain.Parent.Id);
                    parentDomain.SubDomains.Add(domain);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Deletes the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void DeleteDomain(Domain domain)
        {
            using (var context = new LibraryContext())
            {
                if (context.Domain.Contains(domain))
                {
                    context.Domain.Remove(domain);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete domain with id {domain.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all domains.
        /// </summary>
        /// <returns>All domains.</returns>
        public IList<Domain> GetAllDomains()
        {
            using (var context = new LibraryContext())
            {
                return context.Domain
                    .Include(x => x.SubDomains)
                    .Include(x => x.Books)
                    .Select(f => f).ToList();
            }
        }

        /// <summary>
        /// Updates the domain.
        /// </summary>
        /// <param name="domain">The domain.</param>
        public void UpdateDomain(Domain domain)
        {
            using (var context = new LibraryContext())
            {
                if (context.Domain.Contains(domain))
                {
                    context.Domain.Update(domain);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update domain with id {domain.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the domain by given id.
        /// </summary>
        /// <param name="id">The domain id.</param>
        /// <returns>The domain or null if there is no domain with given id.</returns>
        public Domain GetDomainById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Domain
                    .Include(x => x.SubDomains)
                    .Include(x => x.Books)
                    .FirstOrDefault(domain => domain.Id == id);
            }
        }
    }
}