﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LibraryContext.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Library context class.
    /// </summary>
    public class LibraryContext : DbContext
    {
        /// <summary>
        /// Gets or sets Author database set.
        /// </summary>
        public virtual DbSet<Author> Author { get; set; }

        /// <summary>
        /// Gets or sets Book database set.
        /// </summary>
        public virtual DbSet<Book> Book { get; set; }

        /// <summary>
        /// Gets or sets BookAuthor database set.
        /// </summary>
        public virtual DbSet<BookAuthor> BookAuthor { get; set; }

        /// <summary>
        /// Gets or sets BookDomain database set.
        /// </summary>
        public virtual DbSet<BookDomain> BookDomain { get; set; }

        /// <summary>
        /// Gets or sets Constants database set.
        /// </summary>
        public virtual DbSet<Constants> Constants { get; set; }

        /// <summary>
        /// Gets or sets Domain database set.
        /// </summary>
        public virtual DbSet<Domain> Domain { get; set; }

        /// <summary>
        /// Gets or sets Edition database set.
        /// </summary>
        public virtual DbSet<Edition> Edition { get; set; }

        /// <summary>
        /// Gets or sets Library database set.
        /// </summary>
        public virtual DbSet<Library> Library { get; set; }

        /// <summary>
        /// Gets or sets Reader database set.
        /// </summary>
        public virtual DbSet<Reader> Reader { get; set; }

        /// <summary>
        /// Gets or sets EditionReader database set.
        /// </summary>
        public virtual DbSet<EditionReader> EditionReader { get; set; }

        /// <summary>
        /// Configure database.
        /// </summary>
        /// <param name="optionsBuilder">Options builder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;database=ProiectAsse;user=root;password=");
        }

        /// <summary>
        /// Creating the models and the relationships between them.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Author>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Firstname).IsRequired();
                entity.Property(e => e.Lastname).IsRequired();
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Title).IsRequired();
            });

            modelBuilder.Entity<Constants>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.DOM).IsRequired();
                entity.Property(e => e.NMC).IsRequired();
                entity.Property(e => e.PER).IsRequired();
                entity.Property(e => e.C).IsRequired();
                entity.Property(e => e.D).IsRequired();
                entity.Property(e => e.L).IsRequired();
                entity.Property(e => e.LIM).IsRequired();
                entity.Property(e => e.DELTA).IsRequired();
                entity.Property(e => e.NCZ).IsRequired();
            });

            modelBuilder.Entity<Domain>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired();
                entity.HasMany(e => e.SubDomains).WithOne(p => p.Parent).HasForeignKey(x => x.ParentId);
            });

            modelBuilder.Entity<Edition>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Number).IsRequired();
                entity.Property(e => e.Year).IsRequired();
                entity.Property(e => e.Publisher).IsRequired();
                entity.Property(e => e.NumberOfPages).IsRequired();
                entity.Property(e => e.Type).IsRequired();
                entity.HasOne(e => e.Book).WithMany(m => m.Editions).HasForeignKey(x => x.BookId);
                entity.HasOne(e => e.Library).WithOne(m => m.Edition).HasForeignKey<Library>(x => x.EditionId);
            });

            modelBuilder.Entity<Library>(entity =>
            {
                entity.HasKey(e => e.EditionId);
                entity.Property(e => e.TotalNumberOfCopies).IsRequired();
                entity.Property(e => e.NumberOfCopiesReadingRoom).IsRequired();
                entity.Property(e => e.NumberOfBorrowedCopies).IsRequired();
            });

            modelBuilder.Entity<Reader>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Lastname).IsRequired();
                entity.Property(e => e.Firstname).IsRequired();
                entity.Property(e => e.Address).IsRequired();
                entity.Property(e => e.IsLibraryStaff).IsRequired();
                entity.Property(e => e.NumberOfExtensions).IsRequired();
                entity.Property(e => e.JoinDate).IsRequired();
            });

            modelBuilder.Entity<BookAuthor>()
                .HasKey(bc => new { bc.BookId, bc.AuthorId });
            modelBuilder.Entity<BookAuthor>()
                .HasOne(bc => bc.Book)
                .WithMany(b => b.Authors)
                .HasForeignKey(bc => bc.BookId);
            modelBuilder.Entity<BookAuthor>()
                .HasOne(bc => bc.Author)
                .WithMany(c => c.Books)
                .HasForeignKey(bc => bc.AuthorId);

            modelBuilder.Entity<BookDomain>()
                .HasKey(bc => new { bc.BookId, bc.DomainId });
            modelBuilder.Entity<BookDomain>()
                .HasOne(bc => bc.Book)
                .WithMany(b => b.Domains)
                .HasForeignKey(bc => bc.BookId);
            modelBuilder.Entity<BookDomain>()
                .HasOne(bc => bc.Domain)
                .WithMany(c => c.Books)
                .HasForeignKey(bc => bc.DomainId);

            modelBuilder.Entity<EditionReader>()
                .HasKey(bc => new { bc.EditionId, bc.ReaderId });
            modelBuilder.Entity<EditionReader>()
                .HasOne(bc => bc.Edition)
                .WithMany(b => b.Readers)
                .HasForeignKey(bc => bc.EditionId);
            modelBuilder.Entity<EditionReader>()
                .HasOne(bc => bc.Reader)
                .WithMany(c => c.BorrowedEditions)
                .HasForeignKey(bc => bc.ReaderId);
        }
    }
}
