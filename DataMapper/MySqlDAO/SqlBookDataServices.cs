﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlBookDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// SqlBookDataServices class.
    /// </summary>
    public class SqlBookDataServices : IBookDataServices
    {
        /// <summary>
        /// Adds the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void AddBook(Book book)
        {
            using (var context = new LibraryContext())
            {
                context.Book.Add(book);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void DeleteBook(Book book)
        {
            using (var context = new LibraryContext())
            {
                if (context.Book.Contains(book))
                {
                    context.Book.Remove(book);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete book with id {book.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns>All books.</returns>
        public IList<Book> GetAllBooks()
        {
            using (var context = new LibraryContext())
            {
                return context.Book
                    .Include(x => x.Authors)
                    .Include(d => d.Domains)
                    .Include(e => e.Editions)
                    .Select(book => book).ToList();
            }
        }

        /// <summary>
        /// Updates the book.
        /// </summary>
        /// <param name="book">The book.</param>
        public void UpdateBook(Book book)
        {
            using (var context = new LibraryContext())
            {
                if (context.Book.Contains(book))
                {
                    context.Book.Update(book);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update book with id {book.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the author by given id.
        /// </summary>
        /// <param name="id">The author id.</param>
        /// <returns>The author or null if there is no author with given id.</returns>
        public Book GetBookById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Book
                    .Include(x => x.Authors)
                    .Include(d => d.Domains)
                    .Include(e => e.Editions)
                    .FirstOrDefault(book => book.Id == id);
            }
        }
    }
}
