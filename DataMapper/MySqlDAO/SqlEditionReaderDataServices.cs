﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlEditionReaderDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;

    /// <summary>
    /// SqlEditionReaderDataServices class.
    /// </summary>
    public class SqlEditionReaderDataServices : IEditionReaderDataServices
    {
        /// <summary>
        /// Adds the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void AddEditionReader(EditionReader editionReader)
        {
            using (var context = new LibraryContext())
            {
                context.EditionReader.Add(editionReader);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void DeleteEditionReader(EditionReader editionReader)
        {
            using (var context = new LibraryContext())
            {
                context.EditionReader.Remove(editionReader);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets all editionReaders.
        /// </summary>
        /// <returns>All editionReaders.</returns>
        public IList<EditionReader> GetAllEditionReaders()
        {
            using (var context = new LibraryContext())
            {
                return context.EditionReader.Select(bookReader => bookReader).ToList();
            }
        }

        /// <summary>
        /// Updates the editionReader.
        /// </summary>
        /// <param name="editionReader">The editionReader.</param>
        public void UpdateEditionReader(EditionReader editionReader)
        {
            using (var context = new LibraryContext())
            {
                context.EditionReader.Update(editionReader);
                context.SaveChanges();
            }
        }
    }
}