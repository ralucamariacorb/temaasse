﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlConstantsDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;

    /// <summary>
    /// SqlConstantsDataServices class.
    /// </summary>
    public class SqlConstantsDataServices : IConstantsDataServices
    {
        /// <summary>
        /// Adds the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void AddConstants(Constants constants)
        {
            using (var context = new LibraryContext())
            {
                context.Constants.Add(constants);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void DeleteConstants(Constants constants)
        {
            using (var context = new LibraryContext())
            {
                if (context.Constants.Contains(constants))
                {
                    context.Constants.Remove(constants);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete constants with id {constants.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all constants.
        /// </summary>
        /// <returns>All constants.</returns>
        public IList<Constants> GetAllConstants()
        {
            using (var context = new LibraryContext())
            {
                return context.Constants.Select(constants => constants).ToList();
            }
        }

        /// <summary>
        /// Updates the constants.
        /// </summary>
        /// <param name="constants">The constants.</param>
        public void UpdateConstants(Constants constants)
        {
            using (var context = new LibraryContext())
            {
                if (context.Constants.Contains(constants))
                {
                    context.Constants.Update(constants);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update constants with id {constants.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the constants by given id.
        /// </summary>
        /// <param name="id">The constants id.</param>
        /// <returns>The constants or null if there is no constants with given id.</returns>
        public Constants GetConstantsById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Constants.FirstOrDefault(constants => constants.Id == id);
            }
        }

        /// <summary>
        /// Gets the latest constants.
        /// </summary>
        /// <returns>The constants or null if there is no constants.</returns>
        public Constants GetLatestConstants()
        {
            using (var context = new LibraryContext())
            {
                return context.Constants.OrderByDescending(x => x.Id).FirstOrDefault();
            }
        }
    }
}