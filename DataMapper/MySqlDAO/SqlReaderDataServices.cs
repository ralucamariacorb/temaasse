﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlReaderDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;

    /// <summary>
    /// SqlReaderDataServices class.
    /// </summary>
    public class SqlReaderDataServices : IReaderDataServices
    {
        /// <summary>
        /// Adds the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void AddReader(Reader reader)
        {
            using (var context = new LibraryContext())
            {
                context.Reader.Add(reader);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void DeleteReader(Reader reader)
        {
            using (var context = new LibraryContext())
            {
                if (context.Reader.Contains(reader))
                {
                    context.Reader.Remove(reader);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete reader with id {reader.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all readers.
        /// </summary>
        /// <returns>All readers.</returns>
        public IList<Reader> GetAllReaders()
        {
            using (var context = new LibraryContext())
            {
                return context.Reader.Select(reader => reader).ToList();
            }
        }

        /// <summary>
        /// Updates the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void UpdateReader(Reader reader)
        {
            using (var context = new LibraryContext())
            {
                if (context.Reader.Contains(reader))
                {
                    context.Reader.Update(reader);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update reader with id {reader.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the reader by given id.
        /// </summary>
        /// <param name="id">The reader id.</param>
        /// <returns>The reader or null if there is no reader with given id.</returns>
        public Reader GetReaderById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Reader.FirstOrDefault(reader => reader.Id == id);
            }
        }
    }
}