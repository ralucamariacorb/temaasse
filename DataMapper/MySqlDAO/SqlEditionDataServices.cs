﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlEditionDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// SqlEditionDataServices class.
    /// </summary>
    public class SqlEditionDataServices : IEditionDataServices
    {
        /// <summary>
        /// Adds the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void AddEdition(Edition edition)
        {
            using (var context = new LibraryContext())
            {
                if (edition.BookId == null)
                {
                    context.Edition.Add(edition);
                    context.SaveChanges();
                }
                else
                {
                    Book book = context.Book.Include(x => x.Editions).First(x => x.Id == edition.BookId);
                    book.Editions.Add(edition);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Deletes the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void DeleteEdition(Edition edition)
        {
            using (var context = new LibraryContext())
            {
                if (context.Edition.Contains(edition))
                {
                    context.Edition.Remove(edition);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete edition with id {edition.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all editions.
        /// </summary>
        /// <returns>All editions.</returns>
        public IList<Edition> GetAllEditions()
        {
            using (var context = new LibraryContext())
            {
                return context.Edition
                    .Include(x => x.Book)
                    .Include(x => x.Library)
                    .Include(e => e.Readers)
                    .Select(edition => edition).ToList();
            }
        }

        /// <summary>
        /// Updates the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        public void UpdateEdition(Edition edition)
        {
            using (var context = new LibraryContext())
            {
                if (context.Edition.Contains(edition))
                {
                    context.Edition.Update(edition);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update edition with id {edition.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the edition by given id.
        /// </summary>
        /// <param name="id">The edition id.</param>
        /// <returns>The edition or null if there is no edition with given id.</returns>
        public Edition GetEditionById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Edition
                    .Include(x => x.Book)
                    .Include(x => x.Library)
                    .Include(e => e.Readers)
                    .FirstOrDefault(edition => edition.Id == id);
            }
        }
    }
}