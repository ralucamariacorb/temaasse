﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlBookDomainDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;

    /// <summary>
    /// SqlBookDomainDataServices class.
    /// </summary>
    public class SqlBookDomainDataServices : IBookDomainDataServices
    {
        /// <summary>
        /// Adds the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void AddBookDomain(BookDomain bookDomain)
        {
            using (var context = new LibraryContext())
            {
                context.BookDomain.Add(bookDomain);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void DeleteBookDomain(BookDomain bookDomain)
        {
            using (var context = new LibraryContext())
            {
                context.BookDomain.Remove(bookDomain);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets all bookDomains.
        /// </summary>
        /// <returns>All bookDomains.</returns>
        public IList<BookDomain> GetAllBookDomains()
        {
            using (var context = new LibraryContext())
            {
                return context.BookDomain.Select(bookDomain => bookDomain).ToList();
            }
        }

        /// <summary>
        /// Updates the bookDomain.
        /// </summary>
        /// <param name="bookDomain">The bookDomain.</param>
        public void UpdateBookDomain(BookDomain bookDomain)
        {
            using (var context = new LibraryContext())
            {
                context.BookDomain.Update(bookDomain);
                context.SaveChanges();
            }
        }
    }
}