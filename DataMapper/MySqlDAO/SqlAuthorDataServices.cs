﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlAuthorDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// SqlAuthorDataServices class.
    /// </summary>
    public class SqlAuthorDataServices : IAuthorDataServices
    {
        /// <summary>
        /// Adds the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void AddAuthor(Author author)
        {
            using (var context = new LibraryContext())
            {
                context.Author.Add(author);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void DeleteAuthor(Author author)
        {
            using (var context = new LibraryContext())
            {
                if (context.Author.Contains(author))
                {
                    context.Author.Remove(author);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete author with id {author.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all authors.
        /// </summary>
        /// <returns>All authors.</returns>
        public IList<Author> GetAllAuthors()
        {
            using (var context = new LibraryContext())
            {
                return context.Author.Include(x => x.Books).Select(author => author).ToList();
            }
        }

        /// <summary>
        /// Updates the author.
        /// </summary>
        /// <param name="author">The author.</param>
        public void UpdateAuthor(Author author)
        {
            using (var context = new LibraryContext())
            {
                if (context.Author.Contains(author))
                {
                    context.Author.Update(author);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update author with id {author.Id} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the author by given id.
        /// </summary>
        /// <param name="id">The author id.</param>
        /// <returns>The author or null if there is no author with given id.</returns>
        public Author GetAuthorById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Author.Include(x => x.Books).FirstOrDefault(author => author.Id == id);
            }
        }
    }
}
