﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlDAOFactory.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    /// <summary>
    /// SqlDAOFactory class.
    /// </summary>
    public class SqlDAOFactory : IDAOFactory
    {
        /// <summary>
        /// Gets AuthorDataServices.
        /// </summary>
        public IAuthorDataServices AuthorDataServices => new SqlAuthorDataServices();

        /// <summary>
        /// Gets BookAuthorDataServices.
        /// </summary>
        public IBookAuthorDataServices BookAuthorDataServices => new SqlBookAuthorDataServices();

        /// <summary>
        /// Gets BookDataServices.
        /// </summary>
        public IBookDataServices BookDataServices => new SqlBookDataServices();

        /// <summary>
        /// Gets BookDomainDataServices.
        /// </summary>
        public IBookDomainDataServices BookDomainDataServices => new SqlBookDomainDataServices();

        /// <summary>
        /// Gets EditionReaderDataServices.
        /// </summary>
        public IEditionReaderDataServices EditionReaderDataServices => new SqlEditionReaderDataServices();

        /// <summary>
        /// Gets ConstantsDataServices.
        /// </summary>
        public IConstantsDataServices ConstantsDataServices => new SqlConstantsDataServices();

        /// <summary>
        /// Gets DomainDataServices.
        /// </summary>
        public IDomainDataServices DomainDataServices => new SqlDomainDataServices();

        /// <summary>
        /// Gets EditionDataServices.
        /// </summary>
        public IEditionDataServices EditionDataServices => new SqlEditionDataServices();

        /// <summary>
        /// Gets LibraryDataServices.
        /// </summary>
        public ILibraryDataServices LibraryDataServices => new SqlLibraryDataServices();

        /// <summary>
        /// Gets ReaderDataServices.
        /// </summary>
        public IReaderDataServices ReaderDataServices => new SqlReaderDataServices();
    }
}
