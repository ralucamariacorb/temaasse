﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlBookAuthorDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;

    /// <summary>
    /// SqlBookAuthorDataServices class.
    /// </summary>
    public class SqlBookAuthorDataServices : IBookAuthorDataServices
    {
        /// <summary>
        /// Adds the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void AddBookAuthor(BookAuthor bookAuthor)
        {
            using (var context = new LibraryContext())
            {
                context.BookAuthor.Add(bookAuthor);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void DeleteBookAuthor(BookAuthor bookAuthor)
        {
            using (var context = new LibraryContext())
            {
                context.BookAuthor.Remove(bookAuthor);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets all bookAuthors.
        /// </summary>
        /// <returns>All bookAuthors.</returns>
        public IList<BookAuthor> GetAllBookAuthors()
        {
            using (var context = new LibraryContext())
            {
                return context.BookAuthor.Select(bookAuthor => bookAuthor).ToList();
            }
        }

        /// <summary>
        /// Updates the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        public void UpdateBookAuthor(BookAuthor bookAuthor)
        {
            using (var context = new LibraryContext())
            {
                context.BookAuthor.Update(bookAuthor);
                context.SaveChanges();
            }
        }
    }
}
