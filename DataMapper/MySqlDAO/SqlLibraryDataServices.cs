﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlLibraryDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper.MySqlDAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DomainModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// SqlLibraryDataServices class.
    /// </summary>
    public class SqlLibraryDataServices : ILibraryDataServices
    {
        /// <summary>
        /// Adds the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void AddLibrary(Library library)
        {
            using (var context = new LibraryContext())
            {
                context.Library.Add(library);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void DeleteLibrary(Library library)
        {
            using (var context = new LibraryContext())
            {
                if (context.Library.Contains(library))
                {
                    context.Library.Remove(library);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't delete library with id {library.EditionId} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets all librarys.
        /// </summary>
        /// <returns>All librarys.</returns>
        public IList<Library> GetAllLibrarys()
        {
            using (var context = new LibraryContext())
            {
                return context.Library.Include(x => x.Edition).Select(library => library).ToList();
            }
        }

        /// <summary>
        /// Updates the library.
        /// </summary>
        /// <param name="library">The library.</param>
        public void UpdateLibrary(Library library)
        {
            using (var context = new LibraryContext())
            {
                if (context.Library.Contains(library))
                {
                    context.Library.Update(library);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception($"Can't update library with id {library.EditionId} because it's not in the database.");
                }
            }
        }

        /// <summary>
        /// Gets the library by given id.
        /// </summary>
        /// <param name="id">The library id.</param>
        /// <returns>The library or null if there is no library with given id.</returns>
        public Library GetLibraryById(int id)
        {
            using (var context = new LibraryContext())
            {
                return context.Library.Include(x => x.Edition).FirstOrDefault(library => library.EditionId == id);
            }
        }
    }
}