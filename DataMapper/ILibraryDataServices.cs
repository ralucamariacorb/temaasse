﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILibraryDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// ILibraryDataServices interface.
    /// </summary>
    public interface ILibraryDataServices
    {
        /// <summary>
        /// Gets all librarys.
        /// </summary>
        /// <returns>All librarys.</returns>
        IList<Library> GetAllLibrarys();

        /// <summary>
        /// Adds the library.
        /// </summary>
        /// <param name="library">The library.</param>
        void AddLibrary(Library library);

        /// <summary>
        /// Deletes the library.
        /// </summary>
        /// <param name="library">The library.</param>
        void DeleteLibrary(Library library);

        /// <summary>
        /// Updates the library.
        /// </summary>
        /// <param name="library">The library.</param>
        void UpdateLibrary(Library library);

        /// <summary>
        /// Gets the library by given id.
        /// </summary>
        /// <param name="id">The library id.</param>
        /// <returns>The library or null if there is no library with given id.</returns>
        Library GetLibraryById(int id);
    }
}