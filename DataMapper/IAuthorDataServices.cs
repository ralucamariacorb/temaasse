﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthorDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IAuthorDataServices interface.
    /// </summary>
    public interface IAuthorDataServices
    {
        /// <summary>
        /// Gets all authors.
        /// </summary>
        /// <returns>All authors.</returns>
        IList<Author> GetAllAuthors();

        /// <summary>
        /// Adds the author.
        /// </summary>
        /// <param name="author">The author.</param>
        void AddAuthor(Author author);

        /// <summary>
        /// Deletes the author.
        /// </summary>
        /// <param name="author">The author.</param>
        void DeleteAuthor(Author author);

        /// <summary>
        /// Updates the author.
        /// </summary>
        /// <param name="author">The author.</param>
        void UpdateAuthor(Author author);

        /// <summary>
        /// Gets the author by given id.
        /// </summary>
        /// <param name="id">The author id.</param>
        /// <returns>The author or null if there is no author with given id.</returns>
        Author GetAuthorById(int id);
    }
}
