﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBookAuthorDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IBookAuthorDataServices interface.
    /// </summary>
    public interface IBookAuthorDataServices
    {
        /// <summary>
        /// Gets all bookAuthors.
        /// </summary>
        /// <returns>All bookAuthors.</returns>
        IList<BookAuthor> GetAllBookAuthors();

        /// <summary>
        /// Adds the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        void AddBookAuthor(BookAuthor bookAuthor);

        /// <summary>
        /// Deletes the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        void DeleteBookAuthor(BookAuthor bookAuthor);

        /// <summary>
        /// Updates the bookAuthor.
        /// </summary>
        /// <param name="bookAuthor">The bookAuthor.</param>
        void UpdateBookAuthor(BookAuthor bookAuthor);
    }
}
