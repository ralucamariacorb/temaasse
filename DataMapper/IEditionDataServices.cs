﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEditionDataServices.cs" company="Transilvania University of Brasov">
// Copyright (c) Corb Raluca Maria All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataMapper
{
    using System.Collections.Generic;
    using DomainModel;

    /// <summary>
    /// IEditionDataServices interface.
    /// </summary>
    public interface IEditionDataServices
    {
        /// <summary>
        /// Gets all editions.
        /// </summary>
        /// <returns>All editions.</returns>
        IList<Edition> GetAllEditions();

        /// <summary>
        /// Adds the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        void AddEdition(Edition edition);

        /// <summary>
        /// Deletes the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        void DeleteEdition(Edition edition);

        /// <summary>
        /// Updates the edition.
        /// </summary>
        /// <param name="edition">The edition.</param>
        void UpdateEdition(Edition edition);

        /// <summary>
        /// Gets the edition by given id.
        /// </summary>
        /// <param name="id">The edition id.</param>
        /// <returns>The edition or null if there is no edition with given id.</returns>
        Edition GetEditionById(int id);
    }
}